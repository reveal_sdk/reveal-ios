# Reveal Mobile iOS SDK
#### (Version 1.3.26 published 7/13/2017) 
[![Version](https://img.shields.io/cocoapods/v/Reveal.svg?style=flat)](http://cocoadocs.org/docsets/Reveal)
[![License](https://img.shields.io/cocoapods/l/Reveal.svg?style=flat)](http://cocoadocs.org/docsets/Reveal)

Reveal Mobile is a native mobile SDK that allows developers to provide tailored personas to their ad network based on a user's location, beacon interactions and installed applications.

**Swift NOTE: The prebuilt framework version does not support Swift due to legacy compatibility constraints. The easiest way to use the framework in Swift is to install from Cocoapods. We can provide pre-built framework versions supporting iOS 8+ only if Cocoapod installation is not suitable for your implementation.**

## Usage

### Simple Setup

Objective-C
```objc
#import <Reveal/Reveal.h>

// Setup the Reveal Mobile SDK pointed at production with no debug features
Reveal *theSDK = [[Reveal sharedInstance] setupWithAPIKey:<YOUR API KEY>];

// allow the SDK to check and request location permission settings
[theSDK setCanRequestLocationPermission: YES];

// Start the Reveal SDK
[theSDK start];
```

Swift
```Swift
import Reveal
            
// Setup the SDK with your API key
Reveal.sharedInstance().setup( withAPIKey: "<YOUR API KEY>", andServiceType: .production )

// allow the SDK to check and request location permission settings
Reveal.sharedInstance().

// Setup the Reveal Mobile SDK pointed at production with no debug features
Reveal.sharedInstance().start().canRequestLocationPermission = true
```
### Testing Setup
Objective-C
```objc    
Reveal *theSDK = [[Reveal sharedInstance] setupWithAPIKey:<YOUR API KEY>];

// Turn on optional debug logging
theSDK.debug = YES;
    
// Setup a list of beacon UUID's for testing.  This list will override the list from the server if set.
theSDK.debugUUIDs = @[ @"<YOUR BEACON UUID>"];

// allow the SDK to check and request location permission settings
[theSDK setCanRequestLocationPermission: YES];
    
// Start the SDK.  The SDK will contact the server for further config info
// and start monitoring for beacons.
[theSDK start];
```

Swift
```Swift
import Reveal

// Turn on optional debug logging
Reveal.sharedInstance().debug = true
            
// Setup the SDK with your API key
Reveal.sharedInstance().setup( withAPIKey: "<YOUR API KEY>", andServiceType: .production )

// Setup the Reveal Mobile SDK pointed at production with no debug features
Reveal.sharedInstance().start().canRequestLocationPermission = true

// Setup the Reveal Mobile SDK pointed at production with no debug features
Reveal.sharedInstance().start()
```

### Background transmission of beacons

To sending beacon bumps while the app is in background, enable the background fetch capability and add the following to your App delegate file:

Objective-C
```objc
- (void) application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [[Reveal sharedInstance] backgroundFetchWithCompletionHandler: completionHandler];
}
```

Swift
```swift
func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
{
    Reveal.sharedInstance().backgroundFetchWithCompletionHandler( completionHandler );
}
```

### Beacon retrieval 

If you want to be notified about beacons you will need to set the delegate and implement the **foundBeaconOfType:identifier:data:** method as shown below. The beacons will be returned as a dictionary containing all the data. There is currently a Beacon object included in this directory that contains the internal class, you should not use this as it is intended for testing and will be removed in future versions.

NOTE: The delegate method should be set after the beacon service has been created (this will occur as part of start or restart if you have not done so by this time).

```objc

[[Reveal sharedInstance] setDelegate: self];

- (void) foundBeaconOfType:(NSString* _Nonnull) type identifier:(NSString* _Nonnull) identifier data:(NSDictionary* _Nullable) data {

	NSLog( @"beacon data: %@", data );

}
```

### Accessing the Personas

Objective-C
```objc
// Get personas to send to ad network.  Personas array is an array of NSString* objects.
NSArray* personas = [Reveal sharedInstance].personas
```

Swift
```swift
// Get personas to send to ad network.  Personas array is an array of NSString* objects.
var result = Reveal.sharedInstance().personas
```

Accessing the personas is a lightweight call to the SDK that does not involve a network request. You should call for personas every time you build an ad view in order to get the most current available list of personas.

### DFP Integration
If using DoubleClick (DFP), ensure these personas pass to the "reveal" custom targeting key in DFP as a custom parameter. Reveal Mobile will work with your ad trafficking team to create the custom targeting key in DFP. Below is sample code you can insert into your ad call to DFP. *Please, make sure you revise this code sample to fit your existing ad call!* For additional reference beyond the sample code, refer to [Google’s documentation on custom targeting.](https://developers.google.com/mobile-ads-sdk/docs/dfp/ios/targeting#custom_targeting)
```objc
GADAdSize adsize = GADAdSizeFromCGSize(CGSizeMake(300, 250));
    DFPBannerView* ad = [[DFPBannerView alloc] initWithAdSize:adsize];

    GADRequest* request = [GADRequest request];

    // get Reveal personas and add to AdNetworkExtras
    GADExtras *extras = [[[GADExtras alloc] init] autorelease];
    NSMutableDictionary *additionalParameters = [[[NSMutableDictionary alloc] init] autorelease];

    NSArray* personas = [Reveal sharedInstance].personas;
    NSString *persona_list = [personas componentsJoinedByString:@","];
    [additionalParameters setObject:persona_list forKey:@"reveal"];

    extras.additionalParameters = additionalParameters;
    [request registerAdNetworkExtras:extras];

    // get Location and add to request
    CGFloat lat;
    CGFloat lon;
    CGFloat accuracy;

    // populate these variables with the location information from CoreLocation, 
    // then call setLocationWithLatitude:longitude:accuracy:

    [request setLocationWithLatitude:lat longitude:lon accuracy:accuracy];

    // make the request

    [ad loadRequest:request];
```
### Capturing app openings
Reveal Mobile sends information about the user location on each time the application becomes active.  This is done using the following code in the applicationDidBecomeActive: function in your AppDelegate

Objective-C
```objc
// Send current information to the Reveal servers
[[Reveal sharedInstance] restart];
```

Swift
```swift
// Send current information to the Reveal servers
Reveal.sharedInstance().restart()
```

## Project setup
Since Reveal Mobile monitors your users' locations, it is important to communicate clearly to the end user why your application asks for their location.  We suggest setting the following values in your Info.plist file.  You are free to change the wording, ours is only a suggestion.  The first key is for iOS 8.0 support and the latter is for legacy systems.

Key | Value
--- | ---
NSLocationAlwaysUsageDescription | We monitor your iBeacon interactions for advertising purposes.
Privacy - Location Usage Description | We monitor your iBeacon interactions for advertising purposes.

You will also need to add Apple's Core location and Core Bluetooth frameworks to your project and select the "Uses Bluetooth LE accessories" option under the background modes capability.

To support background notification of beacons, you will also need to enable the "Background fetch" fetch capability. 

## Requirements

Reveal Mobile requires at least iOS 7.0 to run.  Beacon scanning requires a Bluetooth LE capable device to operate.

**For beacon scanning to function correctly in iOS, Apple requires the “Always” location permission**. Apple built beacon scanning directly into the operating system, which allows beacon detection to occur while the app is in the background and not currently open. Using the "While Using the App"  setting for location permission isn't sufficient for beacon detection. You must add the "Always" permission, and we recommend this be the default location permission requested.

For reference, here is Apple’s documentation on [beacon scanning](https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/LocationAwarenessPG/RegionMonitoring/RegionMonitoring.html). Here’s where Apple discusses [using Location Services in the background](https://developer.apple.com/library/ios/documentation/CoreLocation/Reference/CLLocationManager_Class/#//apple_ref/doc/uid/TP40007125-CH3-SW63), with the relevant text copied below.

_Using Location Services in the Background: 
Most location services are meant to be used while your app is in the foreground but some can also run in the background. In some cases, location events can even cause your app to be relaunched to process an event. To run most location services in the background, you need to enable the location updates background mode in the Capabilities tab of your Xcode project. For services that launch your app, you need to request (and be granted) “Always” authorization from the user._

Reveal Mobile requires an account with our system and a valid API key.  You may request a production key from the Reveal Mobile team at support@revealmobile.com.

## Installation
### Simple Integration
Reveal Mobile is available through [CocoaPods](http://cocoapods.org). To install it, simply add the following line to your Podfile:

    pod "Reveal"

### Manual Integration
Reveal Mobile can be added as a framework.  First, download the [iOS Framework here](http://revealmobile.com/wp-content/uploads/2016/10/iOS_Reveal.framework-1.3.10.zip). After adding Reveal.framework to your project, you need to link your project against the following system frameworks (CoreLocation, CoreBluetooth, AdSupport).

## Authors

- Reveal Mobile, support@revealmobile.com
- Sean Doherty, sean.doherty@crosscomm.com
- Bobby Skinner, bobby.skinner@crosscomm.com

## License

Reveal Mobile is available under the MIT license. See the LICENSE file for more info.

## Version Log
1.3.10 - 12.06.16 - Support for Swift
1.3.9 - 09.28.16 - Hot fix for non-critical iOS 10 warning
1.3.8 - 09.27.16 - Support for iOS 10
1.3.7 - Internal testing release
1.3.6 - 07.27.16 - Improved beacon detection. Make location timeouts configurable via server.
1.2.30 - 01.3.5 - Internal testing releases
1.2.29 - 05.23.16 - Fixed bug with iOS 7.1 support and Titanium app developers
1.2.28 - 04.27.16 - Restored connection type and UUID parameters to response
1.2.27 - 04.26.16 - Fixed issue with returning beacon bumps in background
1.2.26 - 04.24.16 - Fixed intermittent location issues on startup
1.2.25 - 04.21.16 - Synced iOS and Android handling of securecast codes
1.2.24 - 04.19.16 - Fixed startup location retrieval
1.2.23 - 04.18.16 - Fixed Cocoapod warnings
1.2.21-.22 - Internal testing
1.2.20, 04.09.16 - Additional support for securecast beacons
1.2.18-.19 - Testing
1.2.16 -.17 - 03.29.16 - Support for securecast beacons and BuddyBuild
1.2.15 - Internal testing
1.2.14, 03.22.16 - Fixes intermittent crashes when using stopRangingBeaconsInRegion
1.2.10 - 01.2.13 - Internal testing to rebuild CocoaPods integration
1.2.9, 02.25.16 - Bug fix for location permissions
1.2.8, 02.18.16 - Detect additional beacon types and classes
1.2.7, 02.11.16 - Eddystone beacon detection, normalize iOS & Android data
1.2.6, 01.22.16 - Improving beacon detection with location permissions
1.2.3, 12.17.15 - Additional controls around location permissions
1.2.2, 12.15.15 - Adding caching of personas, improved server communication, match version with Android
1.1.8, 08.13.15 - Updated location fetching away from significant changes only and to a more finely grained query
1.1.7, 08.03.15 - Added controls to support scanning for beacons independent of other app functions
1.1.6, 06.02.15. Updated support for app installation evaluation
1.1.5, 05.09.15. Improved battery life
