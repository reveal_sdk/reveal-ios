# Reveal iOS SDK Release Notes

## Release 1.3.36

##### Completed:

* #161 If no IDFA, act like no location permission (no sending of data)

##### Known issues:

* Bluetooth indicator is red at startup until the first bluetooth cycle begins
* Distance calculation accuracy will vary based on the device. This will improve in future versions
* the beacon delegate may not be called before the Beacon service has been setup

###### Previous releases:

## Release 1.3.34

##### Completed:

* Conditionally compile out SSID collection

## Release 1.3.32

##### Completed:

* Send incomplete beacons on a timer and when memory is low.
**NOTE:**
*You should remove the old test app and re-install it. failure to do so will leave the last saved network visible but displaying the id as "ERROR." While this is harmless, it is not desirable.*

##### Notable Changes:

In order to support sending incomplete beacons when memory is low you need to add a new method to the app delegate:

```objc
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[Reveal sharedInstance] memoryWarning];
}
```

##### Status indicators

There are three new status indicators with this release. These are primarily for my testing at this point but will be improved in the future to have more real world usefulness. On the phone these indicators simply show the state. However on the iPad, you can tap them and get more information - currently this is just the information i need in my testing, but in the future this will be more user friendly.

These are only shown in the beacons page.

###### Location

The location indicator will indicate if you have been receiving location updates. This will change to not receiving if at any point you cease receiving them. It is normal for it to be negative until the first update is received.

###### Bluetooth

The bluetooth indicator actually represents bluetooth beacons and it indicates the detection of beacons. This will be negative for some time. **In future versions this will indicate ability to scan not beacons encountered.** Since this is currently representing beacons encountered, it is normal for it to be negative log after the others have updated.

###### Web

This indicator shows whether or not you are able to communicate with the reveal server.


## Release 1.3.31

##### Completed:

* Fixed first time startup issue
* Added status indicators

## Release 1.3.31

##### Completed:

* Fixed first time startup issue
* Added status indicators

## Release 1.3.28

##### Completed:

* This is a preview for testing only, does not always start bluetooth monitoring after first launch, status icons are not entirely accurate nor are the aesthetically pleasing.

## Release 1.3.26

##### Completed:

* 142 Change location permission handling at startup

##### Notable Changes:

###### Startup Options View

This version introduces a new test screen that allows you to test in one of many configurations. You can select the server to use as well as specify the location permission testing type.

The types are:

* Normal: This tests using the new canRequestLocationPermission flag to operate in the manner similar to the way earlier SDK's were configured.
* Early: This mode will init the SDK, then after a very short delay (5ms) request location permissions to simulate a fast multithreading race condition.
* Late: This mode will init the SDK then wait 30 seconds before requesting location permission
* Never: This mode will never request permission (though it will operate if you have previously granted permission.

**Important: These modes simulate different ways the user may implement their location permissions. We will still use the existing permissions if they have been previously granted. So in order to test these items you will likely have to uninstall the app between each test.**

###### Documentation changes
This version requires you to either get location permissions externally, or to set the canRequestLocationPermission flag to allow the SDK to detect the and possibly request information when the start method is called. So the documentation should be altered to indicate the following:

> ### Simple Setup
> Objective-C
> 
> ```objc
> #import <Reveal/Reveal.h>
> 
> // Setup the Reveal Mobile SDK pointed at production with no debug features
> Reveal *theSDK = [[Reveal sharedInstance] setupWithAPIKey:<YOUR API KEY>];
> 
> // allow the SDK to check and request location permission settings
> [theSDK setcanRequestLocationPermission: YES];
> 
> // Start the Reveal SDK
> [theSDK start];
> ```
> 
> Swift
> ```Swift
> import Reveal           
> 
> // Setup the SDK with your API key
> Reveal.sharedInstance().setup( withAPIKey: "<YOUR API KEY>", andServiceType: .production )
> 
> // allow the SDK to check and request location permission settings
> Reveal.sharedInstance().
> 
> // allow the SDK to check and request location permission settings
> Reveal.sharedInstance().canRequestLocationPermission = true
>
> // Setup the Reveal Mobile SDK pointed at production with no debug features
> Reveal.sharedInstance().start()
> ```
> 
> ### Testing Setup
> Objective-C
> 
> ```objc    
> Reveal *theSDK = [[Reveal sharedInstance] setupWithAPIKey:<YOUR API KEY>];
> 
> // Turn on optional debug logging
> theSDK.debug = YES;
> 
> // Setup a list of beacon UUID's for testing.  This list will override the list from the server if set.
> theSDK.debugUUIDs = @[ @"<YOUR BEACON UUID>"];
>
> // allow the SDK to check and request location permission settings
> [theSDK setCanRequestLocationPermission: YES]; 
> 
> // Start the SDK.  The SDK will contact the server for further config info 
> // and start monitoring for beacons. 
> [theSDK start]; 
> ```
> Swift
> 
> ```Swift
> import Reveal
> 
> // Turn on optional debug logging 
> Reveal.sharedInstance().debug = true           
> 
> // Setup the SDK with your API key
> Reveal.sharedInstance().setup( withAPIKey: "<YOUR API KEY>", andServiceType: .production )
> 
> // allow the SDK to check and request location permission settings
> Reveal.sharedInstance().canRequestLocationPermission = true
> 
> // Setup the Reveal Mobile SDK pointed at production with no debug features
> Reveal.sharedInstance().start()
> ```


## Release 1.3.23

##### Completed:

* 119 Configuration option for selecting API endpoint

## Release 1.3.22

##### Completed:

* 122 Leave beacon not reported

## Release 1.3.20

##### Completed:

* 118 False Beacon report at startup

## Release 1.3.19

##### Completed:

* Added new scanner to get some iBeacons without having to include them in the 20 beacon list
* added black_list tag to allow a list of manufacturers to exclude from presenting as beacons

## Release 1.3.18

##### Completed:

* Dwell time in seconds
* Skipped some revisions to match one out android release

Implement the RVLBeaconDelegate protocol and use foundBeaconOfType to be notified of new beacons. The data for the beacon will be available in the data dictionary. Setup the delegate **AFTER** shortly after calling the SDK Start method (calling it before the BeaconService initializes will not work), this object is created during the startup process.

**NOTE: The current version returns the internal beacon object in the "beacon" key, this is for use by the test application in development and will eventually be removed.**

```objective-c

@interface MyClass: NSObject <RVLBeaconDelegate>

@end

@implementation MyClass

- (void) setupDelegate {
	[[Reveal sharedInstance] setDelegate: self];
}

- (void) foundBeaconOfType:(NSString* _Nonnull) type identifier:(NSString* _Nonnull) identifier data:(NSDictionary* _Nullable) data {
    NSLog( @"Found %@ %@:\n%@", type, identifier, data ); 
}

@end
```

## Release 1.3.14

##### Completed:

* 101 Allow operation without background permissions 

## Release 1.3.13

##### Completed:

* 102 Implement addition of WiFi data to packets on all events

The batch event has been modified to returrn the current SSD and BSSID fields for the active network
```json
{
	...
	"beacons":
	[
		{
			"currentSSID":"CrossComm",
			"currentBSSID":"82:2a:a8:4b:5c:52",
			,"beacon_type":"PebbleBee",
			...
		}
	]
	...
}
```

## Release 1.3.12

##### Completed:

* 101 Allow operation without background permissions 
* 77 Investigate WIFI data we can access

     *preliminary data returned in /batch API call under the key "networks"*
     
	 ***NOTE: This may change moving forward, and should be viewed as a R&D only feature for now***
     
```json
{
	...
	"networks":
	[
	    {
		    "address":
		    {
			    "street":"123 Main Street",
			    "city":"Cary",
			    "state":"NC,
			    "zip":"27513",
			    "country":"US"
		    },
		    "SSID":"myNetwork",
		    "BSSID":"ac:b3:13:c5:0b:25"
	    }
	]
	...
}
```

## Release 1.3.11

##### Completed:

* 99 Beacon property inconsistencies 
* Statistics added to sample application

## Release 1.3.10

##### Completed:

* 86 Collect batch events

## Release 1.3.9

##### Completed:

* 85 API MISUSE log on iOS 10


## Release 1.3.8

##### Completed:

* 83 iOS10 compatability

## Release 1.3.7

##### Completed:

* 66 Wait for closer proximity to beacons
* 79 Improve proximity detection

## Release 1.3.5

##### Completed:

* 64 Support for Tile Beacons
* 65 Make location timeouts configurable from server
* 72 Estimote beacons not detected if configured for eddystone mode

## Release 1.3.2

##### Completed:
* Switched Places radius to 20ft

## Release 1.3.1

##### Completed:
* 58 Create beacon listeners accessible to SDK.
* 62 Implement "Places" on iOS test app for data discovery

## Release 1.3.0

##### Completed:
* 23 Detect Gimbal beacons

## Release 1.2.30

##### Completed:
* 45 Add an attribute to check whether location sharing is enabled.
* 54 iBeacons not being captured
* 55 Core Bluetooth methods called when unsupported

## Release 1.2.29

##### Completed:
* Support for iOS back to 7.1 
* 50 Fix issue on iOS buddy build environment params pass through.


## Release 1.2.28

##### Completed:
* 43 Add conn_type to the info request


## Release 1.2.27

##### Completed:
* 40 Eddystone reported multiple times
* 41 iBeacons monitoring not restarted when ranging stopped

## Release 1.2.26

##### Completed:
* 39 Send address on /info calls too

## Release 1.2.25

##### Completed:
* 37 Remove securecast_manufacturer_codes from iOS version

## Release 1.2.24

##### Completed:
* Fixed startup beacon discovery/location retrieval race condition

## Release 1.2.23

##### Completed:
* Fixed cocoapod warnings and updated to recommended settings

## Release 1.2.22

##### Completed:
* 34 We need to incorporate securecast_manufacturer_codes to select which securecast beacons to search for

## Release 1.2.17

* Secure cast payload fixed and vender key resolution

## Release 1.2.16

* 21 Integrate with build automation using BuddyBuild or similar.
* Added secure case catch all support

## Release 1.2.11

* 29 Crash in stopRangingBeaconsInRegion:
* 30 Crash in RVLWebServices sendNotificationOfBeacon:
* 33 Swirl beacon should use the combined key and instance fields as reveal_beacon_key

## Release 1.2.10

##### Completed:
* 28 Send the SDK version on every event
* 25 Verify and match beacon information between iOS and Android

## Release 1.2.9

##### Completed:
* 9 Setup while in use notification (remove always)

## Release 1.2.8

##### Completed:
* 24 Parse Eddystone and Swirl Beacons for information on iOS

## Release 1.2.7

##### Completed:
* 4 Eddystone integration (RAW)
* 16 Detect Swirl beacons (RAW)
* 17 Improve the UUID caching mechanism to include timestamp
* 18 Clean up and normalize data being sent on info and bump events

## Release 1.2.6

##### Completed:

* 1 remove ranging code
* 2 Add connection type to response
* 5 Store beacons to prefs
* 6 Delay sending of beacon until location either success or failure
* 8 convert major/minor to strings
* 10 enable and test bit code
