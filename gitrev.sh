#!/usr/bin/env bash

function parse_git_dirty() 
{
	git diff --quiet --ignore-submodules HEAD 2>/dev/null; [ $? -eq 1 ] && echo "+"
}

function parse_git_branch() 
{
	git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/\1/"
}

function parse_git_original_hash() 
{
	git rev-parse --short HEAD 2> /dev/null | sed "s/\(.*\)/@\1/"
}

function parse_git_hash() 
{
	git rev-parse --short HEAD 2> /dev/null
}

function processArguments()
{
	TYPE="c"
	# for a in ${BASH_ARGV[*]} ; do
	# 	echo "// arg: $a"
	# 	if [[ $a=="-j" ]]; then
	# 		echo "// it's java"
	# 		TYPE="java"
	# 	elif [[ $a=="-s" ]]; then
	# 		echo "// it's swift"
	# 		TYPE = "swift"
	# 	fi
	# 	echo "// arg after: $a"
	# done
}

processArguments

echo "// Auto generating $TYPE source"

# Get git version number
GIT_BRANCH=$(parse_git_branch)
GIT_HASH=$(parse_git_hash)
GIT_INFO="$GIT_BRANCH $GIT_HASH$(parse_git_dirty)"

echo 
echo "#ifndef GIT_GEN_HEADER"
echo "#define GIT_GEN_HEADER"
echo 
echo "#define GIT_BRANCH @\"$GIT_BRANCH\""
echo "#define GIT_HASH @\"$GIT_HASH\""
echo "#define GIT_INFO @\"$GIT_INFO\""
echo
echo "#endif"