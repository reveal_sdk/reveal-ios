#
# Be sure to run `pod lib lint Reveal.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Reveal"
  s.version          = "1.3.39"
  s.summary          = "Reveal Mobile provides audience understanding for your mobile apps."
  s.description      = <<-DESC
		       	Reveal Mobile is a mobile audience platform.  We provide app publishers with the ability to understand the demographics, behaviors, interests, and political leanings of their mobile app audience.
                       DESC
  s.homepage         = "http://revealmobile.com"
  s.license          = 'MIT'
  s.author           = { "Reveal Mobile" => "support@revealmobile.com", "Sean Doherty" => "sean.doherty@crosscomm.net" }
  s.source           = { :git => "https://bitbucket.org/reveal_sdk/reveal-ios.git", :tag => s.version.to_s }
  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Classes'
  s.frameworks = 'CoreBluetooth', 'CoreLocation', 'AdSupport'
end
