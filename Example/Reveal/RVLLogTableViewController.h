//
//  RVLLogTableViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 11/7/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLLogEntry : NSObject

@property (nonatomic, strong, nonnull) NSString* type;
@property (nonatomic, strong, nonnull) NSString* message;
@property (nonatomic, strong, nonnull) NSDate* timestamp;
@property (nonatomic, strong, nonnull) UIColor* color;

@end

@interface RVLLogTableViewController : UITableViewController

@property (nonatomic, strong, nullable) NSString* type;
@property (nonatomic, strong, nullable) NSString* searchString;
@property (nonatomic, assign) BOOL reversed;

+ (void) startLogging;

- (NSOrderedSet* _Nullable) types;
- (void) refreshTable;

@end
