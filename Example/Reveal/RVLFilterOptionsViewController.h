//
//  RVLFilterOptionsViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 11/8/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RVLLogTableViewController;

@interface RVLFilterOptionsViewController : UIViewController

@property (nonatomic, weak, nullable) RVLLogTableViewController* parent;

@end
