//
//  RVLStatusPopupViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 8/22/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLStatusPopupViewController.h"

@interface RVLStatusPopupViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation RVLStatusPopupViewController

- (void) setStatus:(RVLStatus *)status {
    if ( _status != status ) {
        _status = status;
    }
    
    [self update];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self update];
}

- (void) viewWillAppear:(BOOL)animated {
    [self update];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donePressed:(id)sender {
    [self dismissViewControllerAnimated: YES completion:^{
        RVLLogWithType( @"STATE", @"Dismissed status popup" );
    }];
}

- (void) update {
    if ( self.status ) {
        [self.textView setText: [self.status message]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
