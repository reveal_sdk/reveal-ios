//
//  RVLBeaconSetupViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 7/26/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLBeaconSetupViewController : UIViewController <UITextViewDelegate>

@end
