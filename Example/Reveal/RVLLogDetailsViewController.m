//
//  RVLLogDetailsViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 11/8/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLLogDetailsViewController.h"
#import "RVLJSONTableViewController.h"

@interface RVLLogDetailsViewController ()

@property (weak, nonatomic) IBOutlet UITextView *headerText;
@property (weak, nonatomic) IBOutlet UITextView *detailText;

@end

@implementation RVLLogDetailsViewController

@synthesize logData = _logData;

- (void) setLogData:(RVLLogEntry *)logData
{
    _logData = logData;
    
    [self updateTextViews];
}

- (void) updateTextViews
{
    [[self headerText] setText: [NSString stringWithFormat: @"%@ - %@",
                                 [[self logData] type], [[self logData] timestamp]]];
    
    [[self detailText] setText: [[self logData] message]];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    [self updateTextViews];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ( [segue.destinationViewController isKindOfClass: [RVLJSONTableViewController class]] ) {
        RVLJSONTableViewController* destination = (RVLJSONTableViewController*) segue.destinationViewController;
        
        [destination setJSONString: [[self logData] message]];
    }
}

@end
