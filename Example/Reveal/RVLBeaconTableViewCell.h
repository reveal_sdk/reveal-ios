//
//  RVLBeaconTableViewCell.h
//  Reveal
//
//  Created by Bobby Skinner on 1/24/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLBeaconTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *identifierLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *extrasLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end
