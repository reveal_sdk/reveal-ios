//
//  RVLLogManager.h
//  Reveal
//
//  Created by Bobby Skinner on 8/1/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface RVLLogManager : NSObject

@property (strong, nonatomic, nonnull) NSString* tag;
@property (strong, nonatomic, nonnull) NSString* idfa;
@property (strong, nonatomic, nonnull) NSString* key;
@property (assign, nonatomic) BOOL enabled;

+ (RVLLogManager *_Nonnull)shared;

- (void) start;

- (void) log:(NSString*_Nonnull)message;
- (void) log:(NSString*_Nonnull)message group:(NSString*_Nonnull)group;
- (void) log:(NSString*_Nonnull)message group:(NSString*_Nonnull)group type:(NSString*_Nonnull)type;

- (void) addGroup:(NSString*_Nonnull)name;
- (void) removeAllGroups;

@end
