//
//  RVLLogDetailsViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 11/8/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVLLogTableViewController.h"

@interface RVLLogDetailsViewController : UIViewController

@property (nonatomic, strong) RVLLogEntry* logData;

@end
