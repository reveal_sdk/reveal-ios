//
//  RVLDebugParameterViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 8/31/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLDebugParameterViewController.h"
#import <Reveal.h>
#import "RVLBeaconTest.h"

@interface RVLDebugParameterViewController ()

@property (weak, nonatomic) IBOutlet UILabel *powerLabel;
@property (weak, nonatomic) IBOutlet UILabel *multiplierLabel;
@property (weak, nonatomic) IBOutlet UILabel *exponentLabel;
@property (weak, nonatomic) IBOutlet UILabel *constantLabel;
@property (weak, nonatomic) IBOutlet UISlider *powerSlider;
@property (weak, nonatomic) IBOutlet UISlider *multiplierSlider;
@property (weak, nonatomic) IBOutlet UISlider *exponentSlider;
@property (weak, nonatomic) IBOutlet UISlider *constantSlider;
@property (weak, nonatomic) IBOutlet UIButton *check1SateButton;
@property (weak, nonatomic) IBOutlet UIButton *checkAllStatesButton;

@property (nonatomic, assign) NSInteger nextBeacon;

@end

@implementation RVLDebugParameterViewController

- (IBAction)donePressed:(id)sender
{
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (IBAction)sliderChanged:(id)sender
{
    [self loadLabels];
    
    CurveFittedDistanceCalculator* distance = [[Reveal sharedInstance] distanceCalculator];
    
    distance.mCoefficient1 = self.multiplierSlider.value;
    distance.mCoefficient2 = self.exponentSlider.value;
    distance.mCoefficient3 = self.constantSlider.value;
    
    distance.txPower = (int) self.powerSlider.value;
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"CurveFittedDistanceCalculator"
                                                        object: distance];
}

- (IBAction)releaseAllPressed:(id)sender {
    [[RVLDwellManager defaultManager] releaseAll];
}

- (IBAction)checkOnePressed:(id)sender {
    [self requestStateFor: self.nextBeacon++];
    
    if ( self.nextBeacon >= [[[RVLBeaconTest shared] beaconGroupsForSim] count] )
        self.nextBeacon = 0;
}

- (IBAction)checkAllPressed:(id)sender {
    for( int beaconNo=0 ; beaconNo<[[[RVLBeaconTest shared] beaconGroupsForSim] count] ; beaconNo++ )
        [self requestStateFor: beaconNo];
}
- (IBAction)simulateMemoryWarningPressed:(id)sender {
    [[Reveal sharedInstance] memoryWarning];
}

- (void) setSliderPositions
{
    CurveFittedDistanceCalculator* distance = [[Reveal sharedInstance] distanceCalculator];
    
    self.multiplierSlider.value = distance.mCoefficient1;
    self.exponentSlider.value = distance.mCoefficient2;
    self.constantSlider.value = distance.mCoefficient3;
    
    self.powerSlider.value = distance.txPower;
    
    [self loadLabels];
}

- (void) loadLabels
{
    [self.powerLabel setText: [NSString stringWithFormat: @"%d", (int)[self.powerSlider value]]];
    
    [self.multiplierLabel setText: [NSString stringWithFormat: @"%.7f", [self.multiplierSlider value]]];
    [self.exponentLabel setText: [NSString stringWithFormat: @"%.7f", [self.exponentSlider value]]];
    [self.constantLabel setText: [NSString stringWithFormat: @"%.7f", [self.constantSlider value]]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    self.check1SateButton.enabled = [[RVLBeaconTest shared] useSim];
    self.checkAllStatesButton.enabled = [[RVLBeaconTest shared] useSim];
    
    [self setSliderPositions];
}

- (void) requestStateFor:(NSInteger)beaconNumber {
    CLLocationManager* manager = [[RVLBeaconTest shared] locationManager];
    if ( beaconNumber < [[[RVLBeaconTest shared] beaconGroupsForSim] count] ) {
        NSString* regionId = [[[RVLBeaconTest shared] beaconGroupsForSim] objectAtIndex: beaconNumber];
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString: regionId];
        
        if (uuid)
        {
            CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                                              identifier: regionId];
            
            [manager requestStateForRegion: region];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
