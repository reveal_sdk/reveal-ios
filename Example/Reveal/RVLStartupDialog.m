//
//  RVLStartupDialog.m
//  Reveal
//
//  Created by Bobby Skinner on 7/13/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLStartupDialog.h"
#import "Reveal.h"
#import <CoreLocation/CoreLocation.h>
#import "RVLAppDelegate.h"
#import "RVLBeaconTest.h"

@implementation RVLStartupDialog

- (RVLAppDelegate*) appDelegate {
    return (RVLAppDelegate*) [[UIApplication sharedApplication] delegate];
}

- (void) viewWillAppear:(BOOL)animated {
    self.versionLabel.text = [NSString stringWithFormat: @"Version: %@", [[Reveal sharedInstance] version]];
    
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusAuthorizedAlways:
            self.currentPemissionLabel.text = @"You have granted Always permission";
            break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            self.currentPemissionLabel.text = @"You have granted While in use permission";
            break;
            
        case kCLAuthorizationStatusRestricted:
            self.currentPemissionLabel.text = @"Location access has been restricted";
            break;
            
        case kCLAuthorizationStatusDenied:
            self.currentPemissionLabel.text = @"Location access has been denied";
            break;
            
        default:
            self.currentPemissionLabel.text = @"Current in an unknown state";
            break;
    }
    
    switch ( [[Reveal sharedInstance] serviceType] ) {
        case RVLServiceTypeSandbox:
            [self.serverSelector setSelectedSegmentIndex: 0];
            break;
            
        case RVLServiceTypeProduction:
            [self.serverSelector setSelectedSegmentIndex: 1];
            // TODO: read accuweather setting here not currently available
            break;
    }
    
    [self.beaconSimSummary setText: [[RVLBeaconTest shared] description]];
}

- (IBAction)startButtonPressed:(UIButton *)sender {
    // TODO: note we are updating the appdelegate but I am not sure if it will
    //       ever be used, so we should clean it up later. It should be put in
    //       one place and called but sean removed it so check with him before
    //       changing it back
    switch ( _serverSelector.selectedSegmentIndex) {
        case 0:
            [[self appDelegate] setServiceType: RVLServiceTypeSandbox];
            [[Reveal sharedInstance] setServiceType: RVLServiceTypeSandbox];
            break;
            
        case 1:
            [[self appDelegate] setServiceType: RVLServiceTypeProduction];
            [[Reveal sharedInstance] setServiceType: RVLServiceTypeProduction];
            break;
            
        case 2:
            [[self appDelegate] setServiceType: RVLServiceTypeProduction];
            [[Reveal sharedInstance] setServiceType: RVLServiceTypeProduction];
            [[self appDelegate] setCustomURL: @"https://import.locarta.co/"];
            [[Reveal sharedInstance] updateAPIEndpointBase: @"https://import.locarta.co/"];
            break;
            
        default:
            break;
    }
    
    // Based on switch, determine whether and when to trigger a location permission request.
    switch ( [[self locationSetupType] selectedSegmentIndex] ) {
        case 0: {
            [[Reveal sharedInstance] setCanRequestLocationPermission:YES];
            break;
        }
        case 1: {
            // delay the start call to simulate customer use
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestLocationPermission];
            });
            break;
        }
        case 2: {
            // delay the start call to simulate customer use
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestLocationPermission];
            });
            break;
        }
    }
    
    [[Reveal sharedInstance] setDelegate: [self appDelegate]];
    
    RVLBeaconTest* test = [RVLBeaconTest shared];
    
    if ( [test useLocalBeaconGroups] ) {
        [[Reveal sharedInstance] setDebugUUIDs: [[RVLBeaconTest shared] beaconGroupsForReveal]];
    }
    
    if ( test.useSim ) {
        [test start];
    }
    else {
        [test stop];
    }
    
    
    // This will test calling our start method on the background thread
    if (self.backgroundThreadSegmentedControl.selectedSegmentIndex == 1) {
        dispatch_async( dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 ), ^{
            [[Reveal sharedInstance] start];
        });
    } else {
        [[Reveal sharedInstance] start];
    }
    
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (void) requestLocationPermission {
    // Simulate the customer asking for core location
    static CLLocationManager* clm = nil;
    
    clm =  [[CLLocationManager alloc] init];
    
    switch ([[Reveal sharedInstance] locationServiceType])
    {
        case RVLLocationServiceTypeAlways:
            if ( [clm respondsToSelector: @selector(requestAlwaysAuthorization)] ) {
                [clm requestAlwaysAuthorization];
            }
            break;
            
        case RVLLocationServiceTypeInUse:
            if ( [clm respondsToSelector: @selector(requestAlwaysAuthorization)] )
                [clm requestWhenInUseAuthorization];
            break;
            
        default:
            NSLog( @"RVLAppDelegate: Locations services must be setup for proper operation");
            break;
    }
}

@end
