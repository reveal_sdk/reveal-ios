//
//  RVLStatisticsTableViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 12/14/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLStatisticsTableViewController.h"
#import "Reveal.h"

@interface RVLStatisticsTableViewController ()

@property (nonatomic, strong, nullable) NSDictionary<NSString*,NSDictionary<NSString*, NSNumber*>*>* stats;
@property (nonatomic, strong, nullable) NSTimer* timer;

@end

NSString *stringFromInterval(NSTimeInterval timeInterval)
{
#define SECONDS_PER_MINUTE (60)
#define MINUTES_PER_HOUR (60)
#define SECONDS_PER_HOUR (SECONDS_PER_MINUTE * MINUTES_PER_HOUR)
#define HOURS_PER_DAY (24)
    
    int days = timeInterval / ( SECONDS_PER_HOUR * HOURS_PER_DAY);
    
    
    // convert the time to an integer, as we don't need double precision, and we do need to use the modulous operator
    int ti = (int) round(timeInterval) % (int) ( SECONDS_PER_HOUR * HOURS_PER_DAY);
    
    return [NSString stringWithFormat:@"%d days %.2d:%.2d:%.2d", days, (ti / SECONDS_PER_HOUR) % HOURS_PER_DAY, (ti / SECONDS_PER_MINUTE) % MINUTES_PER_HOUR, ti % SECONDS_PER_MINUTE];
    
#undef SECONDS_PER_MINUTE
#undef MINUTES_PER_HOUR
#undef SECONDS_PER_HOUR
#undef HOURS_PER_DAY
}

@implementation RVLStatisticsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.stats = [NSDictionary dictionary];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval: 2.0
                                                  target: self
                                                selector: @selector(reload)
                                                userInfo: nil
                                                 repeats: YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    [self reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) reload {
    self.stats = [[Reveal sharedInstance] statistics];
    
    [[self tableView] reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self stats] count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary* data = nil;
    
    switch ( section ) {
        case 0:
            return 1;
            break;
            
        case 1:
            data = self.stats[@"success"];
            break;
            
        case 2:
            data = self.stats[@"failure"];
            break;
            
        default:
            break;
    }
    
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statisticalRow" forIndexPath:indexPath];
    NSDictionary<NSString*, NSNumber*>* data = nil;
    NSTimeInterval seconds = fabs( [[[Reveal sharedInstance] startTime] timeIntervalSinceNow] );
    
    switch ( indexPath.section ) {
        case 1:
            data = self.stats[@"success"];
            break;
            
        case 2:
            data = self.stats[@"failure"];
            break;
            
        default:
            break;
    }
    
    if ( data == nil ) {
        cell.textLabel.text = stringFromInterval( seconds );
        cell.detailTextLabel.text = @"";
    }
    else if ( ( [data count] > 0 ) && ( [data count] > indexPath.row ) ) {
        NSArray<NSString*>* keys = [[data allKeys] sortedArrayWithOptions: NSSortStable
                                                          usingComparator: ^NSComparisonResult(NSString*  _Nonnull obj1, NSString*  _Nonnull obj2) {
                                                              return [obj1.lowercaseString compare: obj2.lowercaseString];
                                                          }];
        
        NSString* key = keys[indexPath.row];
        NSNumber* value = data[key];
        
        NSString* rate = @"";
        
        if ( seconds > 10.0 ) {
            
            NSString* unit = @"day";
            
            float perUnit = value.floatValue / ( (int)(seconds / 86400.0 ) + 1.0);
            
            if ( seconds > 180.0 ) {
                if ( perUnit >= 60.0  ) {
                    unit = @"hour";
                    perUnit = value.floatValue / ((int) (seconds / 3600.0 ) + 1.0);
                }
                
                if ( perUnit >= 200.0 ) {
                    unit = @"min";
                    perUnit = value.floatValue / ( (int) (seconds / 60.0 ) + 1.0);
                }
                
                if ( perUnit >= 1000.0 ) {
                    unit = @"sec";
                    perUnit = value.floatValue / (int) seconds;
                }
            }
            
            rate = [NSString stringWithFormat: @" - %.2f/%@", perUnit, unit];
        }
        
        cell.textLabel.text = key;
        cell.detailTextLabel.text = [NSString stringWithFormat: @"%@%@", value, rate];
    }
    
    return cell;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString* result = @"";
    
    switch ( section ) {
        case 0:
            result = @"Time Since Start";
            break;
            
        case 1:
            result = @"Success";
            break;
            
        case 2:
            result = @"Failure";
            break;
            
        default:
            break;
    }
    
    return result;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
