//
//  RVLBluetoothMonitorTableViewCell.h
//  Reveal
//
//  Created by Bobby Skinner on 2/9/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLBluetoothMonitorTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *services;
@property (weak, nonatomic) IBOutlet UILabel *details;
@property (weak, nonatomic) IBOutlet UILabel *count;

@end
