//
//  RVLJSONTableViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 2/6/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLJSONTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary<NSString*,id>* json;

- (void) setJSONString:(NSString*)jsonString;

@end
