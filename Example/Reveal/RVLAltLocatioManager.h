//
//  RVLAltLocatioManager.h
//  Reveal
//
//  Created by Bobby Skinner on 5/31/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reveal.h"

@interface RVLAltLocatioManager : NSObject <RVLLocationService>

@property (nonatomic, strong) CLLocation *userLocation;

@end
