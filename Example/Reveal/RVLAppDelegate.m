//
//  RVLAppDelegate.m
//  Reveal
//
//  Created by CocoaPods on 01/09/2015.
//  Copyright (c) 2014 StepLeader Digital. All rights reserved.
//

// This rather convoluded set of #defines allows you to pass in environment variables
// for buddy build but not require them if you're not.
//
// Do to the XCode mechanism provided, you will get directive defined as empty instead
// of undefined if there is no environment variable defined. This is seems overly complex
// for such a simple task, but it handles all of the cases as far as I can see.
#ifndef __RVLAPPDELEGATE_H__
#define __RVLAPPDELEGATE_H__

#import "RVLAppDelegate.h"
#import "RVLBeaconTest.h"

#define DO_EXPAND(VAL)  VAL ## 1
#define EXPAND(VAL)     DO_EXPAND(VAL)

#ifndef USE_DEBUG_BEACONS
#define USE_DEBUG_BEACONS               0
#endif

// has a key been provided?
#ifdef REVEAL_API_KEY
//#if !(EMPTY(REVEAL_API_KEY) == 1)
#ifndef API_KEY
#define API_KEY                 EXPAND_STRING(REVEAL_API_KEY)
#endif
//#endif
#endif

// has a key been provided?
//#ifdef REVEAL_LOG_KEY
//#if !(EMPTY(REVEAL_LOG_KEY) == 1)
//#ifndef LOG_API_KEY
//#define LOG_API_KEY                 EXPAND_STRING(REVEAL_LOG_KEY)
//#endif
//#endif
//#endif
//
//#ifndef LOG_API_KEY
//#define LOG_API_KEY                 @""
//#endif

// if no key was created above then use the default
#ifndef API_KEY
// production key
//#define API_KEY                 @"53921489d442b031018220bf"

// sandbox key
#define API_KEY                 @"53921489d442b031018220bf"
#endif
#import "Reveal.h"
#import "RVLAltLocatioManager.h"
#import "RVLLogTableViewController.h"
#import "RVLJSONTools.h"
#import "RVLLogManager.h"
#import <AdSupport/AdSupport.h>

@implementation RVLAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // We setup the SDK here, but startup happens in RVLStartupDialog to simulate the various options in startup
    self.maxBeacons = 50;
//    self.useDebugBeacons = USE_DEBUG_BEACONS;
    self.serviceType = REVEAL_SERVER_TYPE;
    
    self.beacons = [[NSMutableArray alloc] initWithCapacity: self.maxBeacons];
    
    [[RVLLogManager shared] setKey: @"381d4a5a-01ed-45b2-8eaa-1fe319efe9c1"]; // TODO: Remove this from the source - think dashes are breaking it
    
    NSString *idfa = nil;
    if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled])
    {
        idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    }
    
    if ( [idfa length] == 0 ) {
        idfa = [[NSString stringWithFormat: @"%@", [[UIDevice currentDevice] name]] stringByReplacingOccurrencesOfString: @" " withString: @"-"];
    }
    
    [[RVLLogManager shared] setIdfa: idfa];
    
    [[RVLLogManager shared] start];
    [[RVLLogManager shared] addGroup: @"COMM"];
    [[RVLLogManager shared] addGroup: @"STATE"];
    [[RVLLogManager shared] addGroup: @"INIT"];
    [[RVLLogManager shared]setEnabled: YES];
    
    [RVLLogTableViewController startLogging];
    
    // API configuration values
    
    // get api key from an environment variable, if not provited use the default
    self.apiKey = [[[NSProcessInfo processInfo] environment] objectForKey:@"REVEAL_API_KEY"];
    if ( [self.apiKey length] == 0 )
        self.apiKey = API_KEY;
    
    // get the service type from an environment variable or use the default if not a valid one
    RVLServiceType serviceType = REVEAL_SERVER_TYPE;
    NSString *serverType = [[[[NSProcessInfo processInfo] environment] objectForKey:@"REVEAL_SERVER_TYPE"] lowercaseString];
    if ( [serverType isEqualToString: @"production"] )
        serviceType = RVLServiceTypeProduction;
    else if ( [serverType isEqualToString: @"sandbox"] )
        serviceType = RVLServiceTypeSandbox;
    NSLog( @"Starting with API key: \"%@\"", self.apiKey );
    
    // Get a reference to the SDK object
    Reveal *theSDK = [Reveal sharedInstance];
    //theSDK.locationManager = [RVLAltLocatioManager new];
    
    // Turn on debug logging, not for production
    theSDK.debug = YES;
    
    [theSDK setupWithAPIKey: self.apiKey andServiceType: self.serviceType];
    
    if ( [[self customURL] length] )
        [theSDK updateAPIEndpointBase: [self customURL]];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    if ( ![version isEqualToString: [theSDK version]] )
    {
        NSLog( @"\n*********************************************\nSDK version is %@, it should be %@\n*********************************************", [theSDK version], version );
        NSAssert( 1==0, @"Aborting if in debug mode because our packaging is broken" );
    }
    
    [[RVLBeaconTest shared] setUseLocalBeaconGroups: self.useDebugBeacons];
    
    if ( self.useTooManyBeacons ) {
        [[RVLBeaconTest shared] setBeaconGroupsForReveal: @[
                                                            @"B9407F30-F5F8-466E-AFF9-25556B57FE6D",
                                                            @"23538c90-4e4c-4183-a32b-381cfd11c465",
                                                            @"97faaca4-d7f1-416d-a5a4-e922dc6edb29",
                                                            @"40A1EB68-883C-45D7-918B-CAB98350B1B1",
                                                            @"2f234454-cf6d-4a0f-adf2-f4911ba9ffa6",
                                                            @"97FAACA4-D7F1-416D-A5A4-E922DC6EDB29",
                                                            
                                                            // The following UUID's are fake, enable
                                                            // them to force an error in testing
                                                            @"12346789-aaaa-aaaa-aaaa-000000000001",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000002",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000003",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000004",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000005",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000006",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000007",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000008",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000009",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000010",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000011",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000012",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000013",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000014",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000015",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000016",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000017",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000018",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000019",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000020",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000021",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000022",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000023",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000024",
                                                            @"12346789-aaaa-aaaa-aaaa-000000000025"
                                                            ] ];
    }
    else {
        [[RVLBeaconTest shared] setBeaconGroupsForReveal: @[
                                                            @"B9407F30-F5F8-466E-AFF9-25556B57FE6D",
                                                            @"23538c90-4e4c-4183-a32b-381cfd11c465",
                                                            @"97faaca4-d7f1-416d-a5a4-e922dc6edb29",
                                                            @"40A1EB68-883C-45D7-918B-CAB98350B1B1",
                                                            @"2f234454-cf6d-4a0f-adf2-f4911ba9ffa6",
                                                            @"97FAACA4-D7F1-416D-A5A4-E922DC6EDB29",
                                                            @"invalid",
                                                            @"",
                                                            @"42",
                                                            @"*"
                                                            ] ];
    }
    
//    if ( [[RVLBeaconTest shared] useLocalBeaconGroups] )
//        theSDK.debugUUIDs = [[RVLBeaconTest shared] beaconGroupsForReveal];
    
    return YES;
}

- (void) foundBeaconOfType:(NSString* _Nonnull) type identifier:(NSString* _Nonnull) identifier data:(NSDictionary* _Nullable) data {
    //NSLog( @"App delegate Found %@ %@:\n%@", type, identifier, data );
    
    RVLBeacon* beacon = data[@"beacon"];
    
    if ( [beacon isKindOfClass: [RVLGenericEvent class]] ) {
        [self.beacons addObject: beacon];
        while ( [[self beacons] count] > self.maxBeacons )
            [[self beacons] removeObjectAtIndex: 0];
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"BEACON" object: self userInfo: data];
        
        NSMutableString* testData = [NSMutableString string];
        NSDictionary* flattened = [data flattenedDictionary];
        
        for( NSString* key in [flattened.allKeys sortedArrayUsingSelector:@selector(compare:)] ) {
            id value = flattened[key];
            
            [testData appendFormat: @"    %@ <%@>=%@\n", key, [value class], value];
        }
        
        RVLLogWithType( @"TEST", @"[NAME]='foundBeaconOfType' [TYPE]='propertyCompareType' [ID]='%@'\n%@", data[@"beacon_type"], testData );
        
    }
}

- (void) leaveBeaconOfType:(NSString*  _Nonnull) type identifier:(NSString*  _Nonnull) identifier {
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"LOSTBEACON" object: self userInfo: @{@"identifier": identifier}];
    
    RVLLogWithType( @"TEST", @"[NAME]='leaveBeaconOfType' [TYPE]='propertyCompareType' [ID]='%@'\n%@", type, @{@"identifier": identifier, @"beacon_type": type} );
}

- (void) locationDidUpdatedTo:(CLLocation* _Nonnull) newLocation from: (CLLocation* _Nullable) oldLocation {
    self.location = newLocation;
    
    NSLog( @"App delegate got location:\n%@", self.location );
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"LOCATION" object: self userInfo: @{@"location": self.location}];
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    RVLLogWithType( @"COMM", @"Application will resign active" );
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    RVLLogWithType( @"COMM", @"Application did enter background" );
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    RVLLogWithType( @"COMM", @"Application will enter foreground" );
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    RVLLogWithType( @"COMM", @"Application did become active" );
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    RVLLogWithType( @"COMM", @"Application will terminate" );
}

- (void) application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    RVLLogWithType( @"COMM", @"Background fetch in progress" );
    
    [[Reveal sharedInstance] backgroundFetchWithCompletionHandler: completionHandler];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[Reveal sharedInstance] memoryWarning];
    RVLLogWithType( @"COMM", @"Application did receive memory warning" );
}

@end

#endif
