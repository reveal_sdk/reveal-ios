//
//  RVLBluetoothMonitorTableViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 2/9/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLBluetoothMonitorTableViewController : UITableViewController

@property (nonatomic, strong) NSString* sortType;
@property (nonatomic, strong) NSMutableDictionary<NSString*,NSString*>* filters;

- (void) addFilterBy:(NSString*)fieldName forValue:(NSString*)value;

@end
