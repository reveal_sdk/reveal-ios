//
//  RVLAltLocatioManager.m
//  Reveal
//
//  Created by Bobby Skinner on 5/31/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLAltLocatioManager.h"

@implementation RVLAltLocatioManager

@synthesize locationUpdated;
@synthesize locationRetainTime;
@synthesize distanceFilter;
@synthesize useSignifigantChange;
@synthesize useSignifigantChangeInBackground;
@synthesize accuracy;

/**
 *  Start monitoring location services. If your bundle contains the
 *  NSLocationWhenInUseUsageDescription string then requestWhenInUseAuthorization
 *  will be called, otherwise if NSLocationAlwaysUsageDescription is provided
 *  then requestAlwaysAuthorization will be called. If neither string is present
 *  then location services will net be started.
 */
- (void) startLocationMonitoring
{
    NSLog( @"RVLAltLocatioManager.startLocationMonitoring called but we are faking it" );
}

/**
 *  stop monitoring location changes
 */
- (void) stopLocationMonitoring
{
    NSLog( @"RVLAltLocatioManager.stopLocationMonitoring called but we are faking it" );
}

/**
 *  Allows functions that need a valid location to wait for a valid location to be available (placemark if possible)
 *  If there is already a valid location available, then the callback returns immediately, otherwise, the callback waits until
 *  there is a valid location or a timeout, in which case the best location we can find will be used
 *
 *  @param callback The method to call when a valid location is available
 */
- (void) waitForValidLocation:(void (^)(void))callback
{
    NSLog( @"RVLAltLocatioManager.waitForValidLocation called but we are faking it pretending we are in Hawaii" );
    
    self.userLocation = [[CLLocation alloc] initWithLatitude: 21.2824 longitude: -157.8375];
    if ( callback )
        callback();
}

- (void)refreshLocationState {
}


@end
