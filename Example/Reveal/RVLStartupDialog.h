//
//  RVLStartupDialog.h
//  Reveal
//
//  Created by Bobby Skinner on 7/13/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLStartupDialog : UIViewController 
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *serverSelector;
@property (weak, nonatomic) IBOutlet UISegmentedControl *locationSetupType;
@property (weak, nonatomic) IBOutlet UISegmentedControl *backgroundThreadSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *currentPemissionLabel;
@property (weak, nonatomic) IBOutlet UILabel *beaconSimSummary;

- (IBAction)startButtonPressed:(id)sender;

@end
