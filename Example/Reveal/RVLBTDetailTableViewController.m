//
//  RVLBTDetailTableViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 2/16/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLBTDetailTableViewController.h"
#import "RVLBluetoothMonitorTableViewCell.h"
#import "Reveal.h"

#define GATT_BASE                   5
#define GATT_NAME                   @"name"
#define GATT_DATA_TYPE              @"type"

@interface RVLBTDetailTableViewController ()

@property (nonatomic, strong, nullable) NSArray <RevealPDU*>* pdus;

@end

@implementation RVLBTDetailTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setDevice:(RevealBluetoothObject *)device {
    _device = device;
    
    self.pdus = [RevealPDU PDUListFromServiceData: [_device services] andDavertisingData: [_device advertisement]];
    
    [self.tableView reloadData];
}

#pragma mark - Table view delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section )
    {
        case 0:
            return 88.0;
            break;
            
        default:
            return 44.0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch ( section )
    {
        case 0:
            return 0.0;
            break;
            
        default:
            return 28.0;
            break;
    }
}


- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel* result = [[UILabel alloc] initWithFrame: CGRectMake( 0, 0, 320.0, 28 )];
    NSString* key = nil;
    
    switch ( section )
    {
        case 1:
            result.text = @"Advertisement";
            break;
            
        case 2:
            result.text = @"UUIDs";
            break;
            
        case 3:
            result.text = @"Services";
            break;
            
        case 4:
            result.text = @"PDUs";
            break;
            
        default:
            key = self.device.characteristics.allKeys[section - GATT_BASE];
            result.text = [NSString stringWithFormat: @"GATT service %@", key];
            break;
    }
    
    return result;
}

#pragma mark - Table view data source -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger result = 0;
    
    if ( self.device )
        result = GATT_BASE + [[[self device] characteristics] count];
    
     return result;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString* key = nil;
    NSDictionary* data = nil;
    
    switch ( section )
    {
        case 0:
            return 1;
            break;
            
        case 1:
            return [[[self device] advertisement] count];
            break;
            
        case 2:
            return [[[self device] uuids] count];
            break;
            
        case 3:
            return [[[self device] services] count];
            break;
            
        case 4:
            return [[self pdus] count];
            break;
            
        default:
            key = self.device.characteristics.allKeys[section - GATT_BASE];
            data = self.device.characteristics[key];
            return [data count];
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString* key = nil;
    NSDictionary* data = nil;
    
    switch ( indexPath.section )
    {
        case 0:
            cell = [self.tableView dequeueReusableCellWithIdentifier: @"RVLBluetoothMonitorTableViewCell" forIndexPath: indexPath];
            [self configureSummaryCell: (RVLBluetoothMonitorTableViewCell*) cell forItem: self.device];
            break;
            
        case 1:
            cell = [self.tableView dequeueReusableCellWithIdentifier: @"RightDetail" forIndexPath: indexPath];
            [self configureAdvertisementCell: cell forItem: self.device andRow: indexPath.row];
            break;
            
        case 2:
            cell = [self.tableView dequeueReusableCellWithIdentifier: @"Basic" forIndexPath: indexPath];
            [self configureUUIDsCell: cell forItem: self.device andRow: indexPath.row];
            break;
            
        case 3:
            cell = [self.tableView dequeueReusableCellWithIdentifier: @"RightDetail" forIndexPath: indexPath];
            [self configureServiceCell: cell forItem: self.device andRow: indexPath.row];
            break;
            
        case 4:
            cell = [self.tableView dequeueReusableCellWithIdentifier: @"RightDetail" forIndexPath: indexPath];
            [self configurePDUCell: cell forItem: self.device andRow: indexPath.row];
            break;
            
        default:
            key = self.device.characteristics.allKeys[indexPath.section - GATT_BASE];
            data = self.device.characteristics[key];
            cell = [self.tableView dequeueReusableCellWithIdentifier: @"RightDetail" forIndexPath: indexPath];
            [self configureGATTServiceCell: cell forItem: data andRow: indexPath.row];
            break;
    }
    
    
    // Configure the cell...
    
    return cell;
}

- (void) configureAdvertisementCell:(UITableViewCell*)cell forItem:(RevealBluetoothObject*)item andRow:(NSInteger)row
{
    if ( item )
    {
        NSArray* data = [[item advertisement] allKeys];
        NSString* key = data[row];
        
        if ( key )
        {
            cell.textLabel.text = [NSString stringWithFormat: @"%@", key];
            cell.detailTextLabel.text = [NSString stringWithFormat: @"%@", [RevealBluetoothObject data: item.advertisement[key]]];
        }
        
    }
}

- (void) configureUUIDsCell:(UITableViewCell*)cell forItem:(RevealBluetoothObject*)item andRow:(NSInteger)row
{
    if ( item )
    {
        NSString* key = item.uuids[row];
        
        if ( key )
        {
            cell.textLabel.text = [NSString stringWithFormat: @"%@", key];
        }
        
    }
}

- (void) configureServiceCell:(UITableViewCell*)cell forItem:(RevealBluetoothObject*)item andRow:(NSInteger)row
{
    if ( item )
    {
        NSArray* data = [[item services] allKeys];
        NSString* key = data[row];
        
        if ( key )
        {
            cell.textLabel.text = [NSString stringWithFormat: @"%@", key];
            cell.detailTextLabel.text = [NSString stringWithFormat: @"%@", [RevealBluetoothObject data: item.services[key]]];
        }
        
    }
}

- (void) configurePDUCell:(UITableViewCell*)cell forItem:(RevealBluetoothObject*)item andRow:(NSInteger)row
{
    if ( item )
    {
        if ( row < [[self pdus] count] ) {
            RevealPDU* pdu = self.pdus[row];
            
            cell.detailTextLabel.text = [NSString stringWithFormat: @"%02x", (int) [pdu type] & 0xff];
            cell.textLabel.text = [NSString stringWithFormat: @"%@", pdu];
        }
    }
}


- (void) configureGATTServiceCell:(UITableViewCell*)cell forItem:(NSDictionary*)data andRow:(NSInteger)row
{
    if ( data )
    {
        NSString* key = data.allKeys[row];
        CBCharacteristic* srv = data[key];
        
        if ( srv )
        {
            unsigned hexNum = 0;
            NSScanner *scanner = [NSScanner scannerWithString: srv.UUID.UUIDString];
            [scanner scanHexInt: &hexNum];
            NSDictionary* info = [self getGATTInfoFor: (NSInteger) hexNum];
            
            if ( srv.UUID.UUIDString.length > 4 )
                cell.textLabel.text = [NSString stringWithFormat: @"%@", srv.UUID.UUIDString];
            else
                cell.textLabel.text = [NSString stringWithFormat: @"%@", info[GATT_NAME]];
            
            if ( srv.value )
            {
                if ( [info[GATT_DATA_TYPE] isEqualToString: @"string"] )
                {
                    NSString* value = [[NSString alloc] initWithData: srv.value encoding: NSASCIIStringEncoding];
                    cell.detailTextLabel.text = [NSString stringWithFormat: @"%@", value];
                }
                else
                    cell.detailTextLabel.text = [NSString stringWithFormat: @"%@", srv.value];
            }
            else
                cell.detailTextLabel.text = @"";
        }
        
    }
}

- (void) configureSummaryCell:(RVLBluetoothMonitorTableViewCell*)cell forItem:(RevealBluetoothObject*)item
{
    
    if ( item )
    {
        if ( [[[item peripheral] name] length] )
        {
            cell.address.text = [NSString stringWithFormat: @"%@: %@", item.peripheral.name, item.identifier];
        }
        else if ( [[[item beacon] vendorName] length] )
        {
            cell.address.text = [NSString stringWithFormat: @"%@: %@", item.beacon.vendorName, item.identifier];
        }
        else
            cell.address.text = item.identifier;
        
        NSMutableString* text = [NSMutableString string];
        if ( [[item services] count] > 0 )
            cell.imageView.image = [UIImage imageNamed:@"Services-96"];
        
        for( id key in item.uuids )
            [text appendFormat: @"%@ ", key];
        
        if ( [item.services count] > 0 )
            [text appendFormat: @"%ld advertised services", (long) [item.services count]];
        
        cell.services.text = text;
        
        if ( item.beacon )
        {
            cell.details.text = item.beacon.description;
            cell.imageView.image = [UIImage imageNamed:@"Lighthouse Filled-100"];
        }
        else
        {
            cell.details.text = @"";
            
        }
        
        if ( item.dateTime )
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"HH:mm:ss"];
            cell.time.text = [formatter stringFromDate: item.dateTime];
        }
        else
            cell.time.text = @"";
        
        cell.count.text = [NSString stringWithFormat: @"%ld", (long) item.count];
        
    }
}

- (NSDictionary*)getGATTInfoFor:(NSInteger)gattCharNumber
{
    NSMutableDictionary* result = [NSMutableDictionary dictionaryWithCapacity: 2];
    
    switch ( gattCharNumber )
    {
        case 0x2a0f:
            result[GATT_NAME] = @"Local time info";
            result[GATT_DATA_TYPE] = @"unknown";
            break;
            
        case 0x2a23:
            result[GATT_NAME] = @"Spectrum ID";
            result[GATT_DATA_TYPE] = @"unknown";
            break;
            
        case 0x2a24:
            result[GATT_NAME] = @"Model Number";
            result[GATT_DATA_TYPE] = @"string";
            break;
            
        case 0x2a25:
            result[GATT_NAME] = @"Serial number";
            result[GATT_DATA_TYPE] = @"string";
            break;
            
        case 0x2a26:
            result[GATT_NAME] = @"Firmware version";
            result[GATT_DATA_TYPE] = @"string";
            break;
            
        case 0x2a27:
            result[GATT_NAME] = @"Firmware version";
            result[GATT_DATA_TYPE] = @"string";
            break;
            
        case 0x2a28:
            result[GATT_NAME] = @"Software version";
            result[GATT_DATA_TYPE] = @"string";
            break;
            
        case 0x2a29:
            result[GATT_NAME] = @"Manufacturer";
            result[GATT_DATA_TYPE] = @"string";
            break;
            
        case 0x2a2a:
            result[GATT_NAME] = @"IEEE 11073-20601 Regulatory Cert.";
            result[GATT_DATA_TYPE] = @"unknown";
            break;
            
        case 0x2a2b:
            result[GATT_NAME] = @"Current time";
            result[GATT_DATA_TYPE] = @"unknown";
            break;
            
        case 0x2a50:
            result[GATT_NAME] = @"PnP ID";
            result[GATT_DATA_TYPE] = @"unknown";
            break;
            
        case 0x2a6c:
            result[GATT_NAME] = @"email";
            result[GATT_DATA_TYPE] = @"string";
            break;
            
        case 0x2a19:
            result[GATT_NAME] = @"Battery level";
            result[GATT_DATA_TYPE] = @"unknown";
            break;
            
        default:
            result[GATT_NAME] = [NSString stringWithFormat: @"0x%04lX", gattCharNumber];
            result[GATT_DATA_TYPE] = @"unknown";
            break;
    }
    
    return result;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
