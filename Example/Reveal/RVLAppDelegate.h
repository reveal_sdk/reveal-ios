//
//  RVLAppDelegate.h
//  Reveal
//
//  Created by CocoaPods on 01/09/2015.
//  Copyright (c) 2014 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reveal.h"

#define STRINGIFY(str) @#str
#define EXPAND_STRING(str) STRINGIFY(str)
#define NO_OTHER_MACRO_STARTS_WITH_THIS_NAME_
#define IS_EMPTY(name) defined(NO_OTHER_MACRO_STARTS_WITH_THIS_NAME_ ## name)
#define EMPTY(name) IS_EMPTY(name)

#ifndef NO_STARTUP_DIALOG
#define NO_STARTUP_DIALOG           0
#endif

// has a service type been provided? If not assume they want the sandbox
#ifndef REVEAL_SERVER_TYPE
#define REVEAL_SERVER_TYPE      RVLServiceTypeProduction
#elif EMPTY(REVEAL_SERVER_TYPE) == 1
#undef REVEAL_SERVER_TYPE
#define REVEAL_SERVER_TYPE      RVLServiceTypeProduction
#endif

@interface RVLAppDelegate : UIResponder <UIApplicationDelegate, RVLBeaconDelegate>

@property (strong, nonatomic, nullable) UIWindow *window;

@property (strong, nonatomic, nullable) NSMutableArray<RVLGenericEvent*>* beacons;
@property (assign, nonatomic) NSInteger maxBeacons;
@property (strong, nonatomic, nullable) CLLocation* location;
@property (nonatomic, strong, nullable) NSString* apiKey;
@property (assign, nonatomic) RVLServiceType serviceType;
@property (assign, nonatomic) BOOL useDebugBeacons;
@property (assign, nonatomic) BOOL useTooManyBeacons;
@property (assign, nonatomic) BOOL startInBackground;
@property (nonatomic, strong, nullable) NSString* customURL;

@end
