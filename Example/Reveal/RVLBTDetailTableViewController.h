//
//  RVLBTDetailTableViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 2/16/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RevealBluetoothObject;

@interface RVLBTDetailTableViewController : UITableViewController

@property (nonatomic, strong) RevealBluetoothObject* device;

@end
