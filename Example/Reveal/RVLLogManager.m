//
//  RVLLogManager.m
//  Reveal
//
//  Created by Bobby Skinner on 8/1/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLLogManager.h"
#import "LogglyLogger.h"
#import "LogglyFormatter.h"

#import "Reveal.h"

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@interface RVLLogManager ()

@property (strong, nonatomic, nonnull) NSMutableArray<NSString*>* groups;

@end

@implementation RVLLogManager

+ (RVLLogManager *)shared
{
    static RVLLogManager* _sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
            _sharedInstance = [[RVLLogManager alloc] init];
        });
    
    return _sharedInstance;
}

- (instancetype _Nonnull) init {
    self = [super init];
    
    if ( self ) {
        self.tag = @"RevealIOS";
        self.idfa = @"unknown";
        self.groups = [NSMutableArray array];
        
        if ( [self.key length] > 0 )
            self.enabled = YES;
    }
    
    return self;
}

- (void) start {
    LogglyLogger *logglyLogger = [[LogglyLogger alloc] init];
    [logglyLogger setLogFormatter:[[LogglyFormatter alloc] init]];
    logglyLogger.logglyKey = self.key;
    [logglyLogger setLogglyTags: @"RevealIOS"];
    
    // Set posting interval every 15 seconds, just for testing this out, but the default value of 120 seconds is better in apps
    // that normally don't access the network very often. When the user suspends the app, the logs will always be posted.
    logglyLogger.saveInterval = 15;
    
    [DDLog addLogger:logglyLogger];
}

- (void) log:(NSString*_Nonnull)message {
    [self log: message group: @"DEBUG"];
}

- (void) log:(NSString*_Nonnull)message group:(NSString*_Nonnull)group {
    [self log: message group: group type: @"DEBUG"];
}

- (void) log:(NSString*_Nonnull)message group:(NSString*_Nonnull)group type:(NSString*_Nonnull)type {
    if ( self.enabled ) {
        BOOL includeThisLog = YES;
        
        if ( [self.groups count] > 0 ) {
            includeThisLog = NO;
            NSString* lcGroup = [group lowercaseString];
            
            for( NSString* item in self.groups ) {
                if ( [lcGroup isEqualToString: item] )
                    includeThisLog = YES;
            }
        }
        
        if ( includeThisLog ) {
            NSString* lcType = [type lowercaseString];
            
            NSString* logglyMessage = [NSString stringWithFormat: @"IOS-%@ [%@] %@", self.idfa, type, message];
            
            if ( [lcType isEqualToString: @"verbose"] ) {
                DDLogVerbose( logglyMessage );
            }
            else if ( [lcType isEqualToString: @"error"] ) {
                DDLogError( logglyMessage );
            }
            else if ( [lcType isEqualToString: @"warn"] || [lcType isEqualToString: @"warning"] ) {
                DDLogWarn( logglyMessage );
            }
            else {
                DDLogDebug( logglyMessage );
            }
        }
    }
}

- (void) addGroup:(NSString*_Nonnull)name {
    [self.groups addObject: [name lowercaseString]];
}

- (void) removeAllGroups {
    [self.groups removeAllObjects];
}

@end
