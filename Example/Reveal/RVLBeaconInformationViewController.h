//
//  RVLBeaconInformationViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 4/21/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reveal.h"

@interface NSDictionary (stringAdd)

- (NSString* _Nonnull) string;
- (NSString* _Nonnull) stringWithSeperator:(NSString*  _Nonnull)sep;

@end

@interface NSArray (stringAdd)

- (NSString* _Nonnull) string;
- (NSString* _Nonnull) stringWithSeperator:(NSString*  _Nonnull)sep;

@end

@interface RVLBeaconInformationViewController : UIViewController

@property (nonatomic, strong, nullable) RVLBeacon* beacon;
@property (nonatomic, strong, nullable) RevealBluetoothObject* device;

@end
