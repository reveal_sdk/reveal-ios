//
//  RVLLogTableViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 11/7/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLLogTableViewController.h"
#import <Reveal/Reveal.h>
#import "RVLLogDetailsViewController.h"
#import "RVLFilterOptionsViewController.h"
#import "RVLLogManager.h"

#define MAX_LOG_ENTRIES                 2000
#define NOTIFICATION_OF_LOG             @"RVLLogTableViewController::log"

@implementation RVLLogEntry

@end

@interface RVLLogTableViewController ()

@property (nonatomic, strong, nullable) NSArray <RVLLogEntry*>* snapshot;

@end

static NSMutableArray <RVLLogEntry*>* logEntries = nil;

@implementation RVLLogTableViewController

+ (void) startLogging
{
    logEntries = [NSMutableArray arrayWithCapacity: MAX_LOG_ENTRIES];
    
    [[RVLDebugLog sharedLog] setLogMirror:^(NSString *type, NSString *message, UIColor *color)
        {
             RVLLogEntry* entry = [RVLLogEntry new];
             
             entry.timestamp = [NSDate date];
             entry.type = type;
             entry.message = message;
             entry.color = color;
            
            [[RVLLogManager shared] log: message group: type type: type];
             
             @synchronized ( self )
             {
                 while ( [logEntries count] >= MAX_LOG_ENTRIES )
                     [logEntries removeObjectAtIndex: 0];
                 
                 [logEntries addObject: entry];
             }
             
             dispatch_async( dispatch_get_main_queue(), ^
                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_OF_LOG
                                                                            object: nil];
                    });
        }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.type = nil;
    self.reversed = YES;
    self.searchString = nil;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(refreshTable)
                                                 name: NOTIFICATION_OF_LOG
                                               object: nil];
}

- (void) refreshTable
{
    dispatch_async( dispatch_get_main_queue(), ^
       {
           [[self tableView] reloadData];
       });
}

- (NSOrderedSet*) types
{
    NSOrderedSet *result = nil;
    
    if ( logEntries.count > 0 ) {
    NSArray *records = [logEntries valueForKey:@"type"];
    result = [NSOrderedSet orderedSetWithArray: records];
    }
    
    return result;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    @synchronized ( self )
    {
        [logEntries removeAllObjects];
        self.snapshot = nil;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray* temp = nil;
    @synchronized ( self )
    {
        temp = logEntries;
    }
    
    if ( [self.type length] )
        temp = [temp filteredArrayUsingPredicate: [NSPredicate predicateWithFormat: @"type==%@", self.type]];
    
    if ( self.searchString ) {
        NSString*  searcWithWildcards = [NSString stringWithFormat: @"*%@*", self.searchString];
        temp = [temp filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"message LIKE[c] %@", searcWithWildcards]];
    }
    
    self.snapshot = temp;
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self snapshot] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"logCell" forIndexPath:indexPath];
    RVLLogEntry* entry = [self entryForRow: indexPath.row];
    
    cell.textLabel.text = entry.message;
    cell.textLabel.textColor = entry.color;
    
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%@ - %@", entry.type, entry.timestamp];
    
    return cell;
}

- (RVLLogEntry*)entryForRow:(NSInteger)row
{
    NSInteger actualRow = row;
    
    if ( self.reversed )
        actualRow = self.snapshot.count - row - 1;
    
    RVLLogEntry* result = self.snapshot[actualRow];
    
    return result;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    
    if ( [[segue destinationViewController] isKindOfClass: [RVLLogDetailsViewController class]] ) {
        RVLLogDetailsViewController* logView = [segue destinationViewController];
        NSIndexPath* indexPath = [[self tableView] indexPathForSelectedRow];
        
        if ( indexPath ) {
            RVLLogEntry* entry = [self entryForRow: indexPath.row];
            
            logView.logData = entry;
        }
    }
    else if ( [[segue destinationViewController] isKindOfClass: [RVLFilterOptionsViewController class]] ) {
        RVLFilterOptionsViewController* options = [segue destinationViewController];
        options.parent = self;
    }
}

@end
