//
//  RVLBeaconTableViewCell.m
//  Reveal
//
//  Created by Bobby Skinner on 1/24/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLBeaconTableViewCell.h"

@implementation RVLBeaconTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
