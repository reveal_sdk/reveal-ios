//
//  RVLFilterViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 7/11/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLFilterViewController.h"

@interface RVLFilterViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *filterFieldPicker;
@property (weak, nonatomic) IBOutlet UITextField *valueField;

@end

@implementation RVLFilterViewController

- (IBAction)cancelPressed:(id)sender
{
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (IBAction)donePressed:(id)sender
{
    BOOL mayExit = YES;
    NSString* field = nil;
    NSString* value = nil;
    NSInteger selectedItem = [self.filterFieldPicker selectedRowInComponent: 0] - 1;
    
    if ( selectedItem >= 0 )
    {
        field = self.filterFields[selectedItem];
        value = self.valueField.text;
    }
    
    if ( _completed )
        mayExit = _completed( field, value );
    
    if ( mayExit )
        [self dismissViewControllerAnimated: YES completion: nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ( [[self filterFields] count] == 0 )
    {
        self.filterFields = @[@"Bluetooth name", @"UUID", @"Type", @"id"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerView methods -

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[self filterFields] count] + 1;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString* result = @"None";
    
    if ( ( row > 0 ) && ( row <= [[self filterFields] count] ) )
    {
        result = self.filterFields[row-1];
    }
    
    return result;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
