//
//  RVLSelectionTableViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 2/16/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLSelectionTableViewController : UITableViewController

@property (nonatomic, copy) void (^selectionPressed)(id item, NSIndexPath* indexPath);

@end
