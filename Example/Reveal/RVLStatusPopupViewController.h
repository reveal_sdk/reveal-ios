//
//  RVLStatusPopupViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 8/22/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reveal.h"

@interface RVLStatusPopupViewController : UIViewController

@property (nonatomic, strong, nullable) RVLStatus* status;

@end
