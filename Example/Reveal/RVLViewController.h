//
//  RVLViewController.h
//  Reveal
//
//  Created by Sean Doherty on 01/09/2015.
//  Copyright (c) 2014 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Reveal.h>

@interface RVLViewController : UIViewController <UITableViewDataSource, RVLBeaconDelegate, UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong, nullable) RVLStatus* lastSelectedStatus;

@end
