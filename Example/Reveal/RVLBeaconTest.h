//
//  RVLBeaconTest.h
//  Reveal
//
//  Created by Bobby Skinner on 7/26/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>

@interface RVLBeaconTest : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong, nullable) NSArray<NSString*>* beaconGroupsForReveal;
@property (nonatomic, assign) BOOL useLocalBeaconGroups;

@property (nonatomic, assign) BOOL useSim;
@property (nonatomic, strong, nullable) NSArray<NSString*>* beaconGroupsForSim;
@property (nonatomic, assign) BOOL includeLocalBeacons;

@property (nonatomic, assign) NSInteger simBeaconCount;
@property (nonatomic, assign) double agressiveness;

@property (nonatomic, strong, nullable) CLLocationManager *locationManager;

+ (id _Nonnull)shared;

- (void) start;

- (void) stop;

@end
