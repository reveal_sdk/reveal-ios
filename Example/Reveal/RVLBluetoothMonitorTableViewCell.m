//
//  RVLBluetoothMonitorTableViewCell.m
//  Reveal
//
//  Created by Bobby Skinner on 2/9/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLBluetoothMonitorTableViewCell.h"

@implementation RVLBluetoothMonitorTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
