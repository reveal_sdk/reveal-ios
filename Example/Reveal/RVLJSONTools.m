//
//  RVLJSONTools.m
//  Reveal
//
//  Created by Bobby Skinner on 2/6/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLJSONTools.h"

@implementation NSDictionary (RVLJSONTools)

- (void) addToDictionary:(NSMutableDictionary*)result forKey:(NSString*)parent {
    for( NSString* key in self.allKeys ) {
        id value = self[key];
        
        NSMutableString* displayKey = [NSMutableString stringWithString: parent];
        if ( [parent length] > 0 )
            [displayKey appendString: @"."];
        [displayKey appendString: key];
        
        if ( [value isKindOfClass: [NSDictionary class]] ) {
            [(NSDictionary*) value addToDictionary: result forKey: displayKey];
        }
        else if ( [value isKindOfClass: [NSArray class]] ) {
            NSArray* array = (NSArray*) value;
            result[displayKey] = [NSString stringWithFormat: @"<%d element array>", (int) [array count]];
            
            for( int i=0 ; i< [array count] ; i++ ) {
                NSString* arrayKey = [NSString stringWithFormat: @"%@[%03d]", displayKey, i];
                
                id value = array[i];
                
                if ( [value isKindOfClass: [NSDictionary class]] ) {
                    [(NSDictionary*) value addToDictionary: result forKey: arrayKey];
                }
                else {
                    result[arrayKey] = [NSString stringWithFormat: @"%@", value];
                }
            }
        }
        else {
            result[displayKey] = [NSString stringWithFormat: @"%@", value];
        }
    }
}

- (NSDictionary*)flattenedDictionary {
    NSMutableDictionary* result = [NSMutableDictionary dictionary];
    
    [self addToDictionary: result forKey: @""];
    
    return result;
}

@end
