//
//  RVLBluetoothMonitorTableViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 2/9/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved. 
//

#import "RVLBluetoothMonitorTableViewController.h"
#import "RVLBluetoothMonitorTableViewCell.h"
#import "RVLSelectionTableViewController.h"
#import "RVLBTDetailTableViewController.h"
#import "Reveal.h"
#import "RVLFilterViewController.h"

@interface RVLBluetoothMonitorTableViewController ()

@property (nonatomic, strong) NSArray* data;

@property (nonatomic, strong) NSTimer* timer;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *startStopButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *filterButton;

@end

@implementation RVLBluetoothMonitorTableViewController


- (void) addFilterBy:(NSString*)fieldName forValue:(NSString*)value
{
    if ( self.filters == nil )
        self.filters = [NSMutableDictionary new];
    
    self.filters[fieldName] = value;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.timer invalidate];
    self.timer = nil;
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval: 5.0
                                                  target: self
                                                selector: @selector( timerFired:)
                                                userInfo: nil
                                                 repeats: YES];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    [self updateStartStopButton];
}

- (void) updateStartStopButton
{
    if ( [[Reveal sharedInstance] captureAllDevices] )
    {
        [self.startStopButton setTitle: @"Stop"];
        [self.timer invalidate];
        self.timer = nil;
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval: 5.0
                                                      target: self
                                                    selector: @selector( timerFired:)
                                                    userInfo: nil
                                                     repeats: YES];
    }
    else
    {
        [self.startStopButton setTitle: @"Start"];
        [self.timer invalidate];
        self.timer = nil;
        [self.tableView reloadData];
    }
}

- (IBAction)startStopButtonPressed:(id)sender
{
    [[Reveal sharedInstance] setCaptureAllDevices: ![[Reveal sharedInstance] captureAllDevices]];
    
    [self updateStartStopButton];
    
    [self.tableView reloadData];
}

- (IBAction)filterButtonPressed:(id)sender
{
    UIStoryboard* storyBoard = [UIStoryboard storyboardWithName: @"Main_iPad" bundle: nil];
    UINavigationController* modal = [storyBoard instantiateViewControllerWithIdentifier: @"filterAddForm"];
    
    if ( modal )
    {
        RVLFilterViewController * filterView = [modal.viewControllers firstObject];
        
        if ( [filterView isKindOfClass: [RVLFilterViewController class]] )
        {
            [filterView setCompleted:^BOOL(NSString * _Nullable fieldName, NSString * _Nullable value )
            {
                if ( fieldName && value )
                    [self addFilterBy: fieldName forValue:value];
                else
                    [self.filters removeAllObjects];
                
                dispatch_async( dispatch_get_main_queue(), ^
                {
                    [self.tableView reloadData];
                });
                
                return YES;
            }];
        }
        
        [self presentViewController: modal animated: YES completion: nil];
    }
}

- (void) timerFired:(id)sender
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray<RevealBluetoothObject*>*) filteredList
{
    NSMutableArray<RevealBluetoothObject*>* result = [NSMutableArray array];
    
    for( RevealBluetoothObject* item in [[[Reveal sharedInstance] bluetoothDevices] allValues] )
    {
        BOOL keep = YES;
        
        for( NSString* key in self.filters.allKeys )
        {
            NSString* value = self.filters[key];
            
            if ( [key isEqualToString: @"Bluetooth name"] )
            {
                if ( ![item.name.lowercaseString containsString: value.lowercaseString] )
                    keep = NO;
            }
            else if ( [key isEqualToString: @"UUID"] )
            {
                keep = NO;
                
                for( CBUUID* uuid in item.uuids )
                {
                if ( [uuid.UUIDString.lowercaseString containsString: value.lowercaseString] )
                    keep = YES;
                }
            }
            else if ( [key isEqualToString: @"id"] )
            {
                if ( ![item.identifier.lowercaseString containsString: value.lowercaseString] )
                    keep = NO;
            }
        }
        
        if ( keep )
            [result addObject: item];
    }
    
    return result;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( [self.sortType isEqualToString:  @"Date Asscending"] )
    {
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"dateTime" ascending: YES];
        NSSortDescriptor *dateDescriptor2 = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: YES];
        self.data = [[self filteredList] sortedArrayUsingDescriptors: @[dateDescriptor, dateDescriptor2]];
    }
    else if ( [self.sortType isEqualToString:  @"Date Descending"] )
    {
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"dateTime" ascending: NO];
        NSSortDescriptor *dateDescriptor2 = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: NO];
        self.data = [[self filteredList] sortedArrayUsingDescriptors: @[dateDescriptor, dateDescriptor2]];
    }
    else if ( [self.sortType isEqualToString:  @"Count Asscending"] )
    {
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: YES];
        NSSortDescriptor *dateDescriptor2 = [NSSortDescriptor sortDescriptorWithKey: @"dateTime" ascending: YES];
        self.data = [[self filteredList] sortedArrayUsingDescriptors: @[dateDescriptor, dateDescriptor2]];
    }
    else if ( [self.sortType isEqualToString:  @"Count Descending"] )
    {
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: NO];
        NSSortDescriptor *dateDescriptor2 = [NSSortDescriptor sortDescriptorWithKey: @"dateTime" ascending: NO];
        self.data = [[self filteredList] sortedArrayUsingDescriptors: @[dateDescriptor, dateDescriptor2]];
    }
    else if ( [self.sortType isEqualToString:  @"BT Name Asscending"] )
    {
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"name" ascending: YES];
        NSSortDescriptor *dateDescriptor2 = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: YES];
        self.data = [[self filteredList] sortedArrayUsingDescriptors: @[dateDescriptor, dateDescriptor2]];
    }
    else if ( [self.sortType isEqualToString:  @"BT Name Descending"] )
    {
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey: @"name" ascending: NO];
        NSSortDescriptor *dateDescriptor2 = [NSSortDescriptor sortDescriptorWithKey: @"count" ascending: NO];
        self.data = [[self filteredList] sortedArrayUsingDescriptors: @[dateDescriptor, dateDescriptor2]];
    }
    else
        self.data = [self filteredList];
    
    // BT Name Descending
    
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RVLBluetoothMonitorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RVLBluetoothMonitorTableViewCell" forIndexPath:indexPath];
    RevealBluetoothObject* item = self.data[indexPath.row];
    
    cell.imageView.image = nil;
    
    if ( item )
    {
        if ( [[[item peripheral] name] length] )
        {
            cell.address.text = [NSString stringWithFormat: @"%@: %@", item.peripheral.name, item.identifier];
        }
        else if ( [[[item beacon] vendorName] length] )
        {
            cell.address.text = [NSString stringWithFormat: @"%@: %@", item.beacon.vendorName, item.identifier];
        }
        else
            cell.address.text = item.identifier;
        
        NSMutableString* text = [NSMutableString string];
        
        
        
        if ( [item services] ) {
            NSArray<RevealPDU*>* pdus = [RevealPDU PDUListFromServiceData: item.services];
            
            for( RevealPDU* pdu in pdus ) {
                [text appendFormat: @"%@ ", pdu];
            }
        }
        
        if ( item.advertisement ) {
            for( NSString* key in item.advertisement ) {
                id value = item.advertisement[key];
                [text appendFormat: @"PDU-%@: %@ ", key, value];
            }
        }
        
        for( id key in item.services.allKeys )
            [text appendFormat: @"%@=%@ ", key, [RevealBluetoothObject data: item.services[key]]];
        
        if ( [text length] > 0 )
            cell.imageView.image = [UIImage imageNamed:@"Services-96"];
        
        for( id key in item.uuids )
            [text appendFormat: @"%@ ", key];
        
        //[text appendFormat: @"%ld advertised services", [item.services count]];
        
        NSLog( @"Service Data %@: %@", [[item peripheral] name], text );
        cell.services.text = text;
        
        if ( item.beacon )
        {
            cell.details.text = item.beacon.description;
            cell.imageView.image = [UIImage imageNamed:@"Lighthouse Filled-100"];
        }
        else
        {
            text = [NSMutableString string];
            for( id key in item.advertisement.allKeys )
                [text appendFormat: @"%@=%@ ", [RevealBluetoothObject serviceName: key], [RevealBluetoothObject data: item.advertisement[key]]];
            
            cell.details.text = text;
            
        }
        
        if ( item.dateTime )
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"HH:mm:ss"];
            cell.time.text = [formatter stringFromDate: item.dateTime];
        }
        else
            cell.time.text = @"";
        
        if ( [[item characteristics] count] > 0 )
            cell.count.text = [NSString stringWithFormat: @"* %ld", (long) item.count];
        else
            cell.count.text = [NSString stringWithFormat: @"%ld", (long) item.count];
        
    }
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ( [segue.destinationViewController isKindOfClass: [RVLSelectionTableViewController class]] ) {
        RVLSelectionTableViewController* selView = segue.destinationViewController;
        __weak RVLBluetoothMonitorTableViewController* me = self;
        
        [selView setSelectionPressed:^(id item, NSIndexPath *indexPath)
            {
                if ( [item isKindOfClass: [NSString class]] )
                {
                    me.sortType = item;
                    
                    dispatch_async( dispatch_get_main_queue(), ^
                        {
                            [me.tableView reloadData];
                        });
                }
            }];
    }
    else if ( [segue.destinationViewController isKindOfClass: [RVLBTDetailTableViewController class]] )
    {
        RVLBTDetailTableViewController* detailView = segue.destinationViewController;
        
        detailView.device = self.data[[[self.tableView indexPathForSelectedRow] row]];
    }
}

@end
