//
//  RVLBeaconTest.m
//  Reveal
//
//  Created by Bobby Skinner on 7/26/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLBeaconTest.h"
#import "Reveal.h"

@implementation RVLBeaconTest

+ (id)shared {
    static RVLBeaconTest *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
            sharedManager = [[self alloc] init];
        });
    
    return sharedManager;
}

- (NSString*) description {
    NSMutableString* result = [NSMutableString string];
    
    if ( self.useLocalBeaconGroups )
        [result appendFormat: @"%d debug beacons", (int) [self.beaconGroupsForReveal count]];
    
    if ( self.useSim ) {
        if ( [result length] > 0 )
            [result appendString: @", "];
        long count = self.simBeaconCount;
        
        if ( self.includeLocalBeacons )
            count = count +  [self.beaconGroupsForReveal count];
        
        [result appendFormat: @"%ld sim beacons", count];
        
        if ( _agressiveness < 0.1 )
            [result appendString: @" passive"];
        else if ( _agressiveness > 0.9 )
            [result appendString: @" aggressive"];
        else
            [result appendFormat: @" %2.1f%% aggressive", _agressiveness * 100.0];
    }
    
    return result;
}

- (void) start {
    NSMutableArray<NSString*>* beacons = [NSMutableArray array];
        [beacons addObjectsFromArray: self.beaconGroupsForReveal];
    
    for( int count = (int) beacons.count ; count < self.simBeaconCount ; count++ ) {
        NSString* fakeBeacon = [NSString stringWithFormat: @"%08x-%04x-%04x-%04x-%012x", count * 7, count, count * 3, count / 2, count * 13];
        
        [beacons addObject: fakeBeacon];
    }
    
    if ( [beacons count] > 0 )
        self.beaconGroupsForSim = beacons;
    
    CLLocationDistance distanceFilter = 101 - ( self.agressiveness * 100 );
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
            switch ([[Reveal sharedInstance] locationServiceType])
            {
                case RVLLocationServiceTypeAlways:
                            self.locationManager = [[CLLocationManager alloc] init];
                            self.locationManager.delegate = self;
                            self.locationManager.distanceFilter = distanceFilter;
                            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                            self.locationManager.pausesLocationUpdatesAutomatically = NO;
                    
                            [self.locationManager startUpdatingLocation];
                    
                            [self startScanning];
                    break;
                    
                case RVLLocationServiceTypeInUse:
                            self.locationManager = [[CLLocationManager alloc] init];
                            self.locationManager.delegate = self;
                            self.locationManager.distanceFilter = distanceFilter;
                            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                            self.locationManager.pausesLocationUpdatesAutomatically = NO;
                    
                            [self.locationManager startUpdatingLocation];
                    
                            [self startScanning];
                    break;
                    
                default:
                    RVLLogWithType( @"WARN", @"No location permission provided" );
                    break;
            }
    });
}

- (void) stop
{
    [self.locationManager stopUpdatingLocation];
}

- (void) startScanning {
    for( NSString* beaconId in self.beaconGroupsForSim ) {
        [self addBeacon: beaconId];
    }
}

- (void)addBeacon:(NSString *)beaconID
{
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:beaconID];
    
    if (uuid)
    {
        CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                                          identifier:beaconID];
        beaconRegion.notifyEntryStateOnDisplay = YES;
        
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse )
        {
            RVLLogWithType( @"STATE", @"Test Monitoring %@", beaconID );
            [[self locationManager] stopRangingBeaconsInRegion:beaconRegion];
            [[self locationManager] startMonitoringForRegion:beaconRegion];
        }
    }
}

#pragma mark - delegate methods -

- (void)locationManager:(CLLocationManager *)manager
                monitoringDidFailForRegion:(CLRegion *)region
                withError:(NSError *)error
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // If we encounter an error here, we've hit our cap of monitoring
        [self stop];
    });
}

- (void)         locationManager:(CLLocationManager *)manager
  rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region
                       withError:(NSError *)error
{
    //VERBOSE( @"ERROR", @"RVLBeaconManager rangingBeaconsDidFailForRegion %@", error );
}

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons
               inRegion:(CLBeaconRegion *)region
{
    
}
- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
//    static long count = 0;
    RVLLogWithType( @"DEBUG",@"RVLBeaconTest didDetermineState: %d forRegion: %@", (int)state, region );
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ( [region isKindOfClass: [CLBeaconRegion class]] ) {
            if (state == CLRegionStateInside) {
                [self.locationManager stopMonitoringForRegion: region];
                [self.locationManager startRangingBeaconsInRegion: (CLBeaconRegion*)region];
            }
            else if (state == CLRegionStateOutside ) {
                if ( ![[Reveal sharedInstance] useManagedBackgroundMode] ) {
                    [self.locationManager stopRangingBeaconsInRegion: (CLBeaconRegion*)region];
                    [self.locationManager startMonitoringForRegion: (CLBeaconRegion*)region];
                }
            }
            
        }
        
        // bug the manager
//        if ( ++count > 20 ) {
//            dispatch_async(dispatch_get_main_queue(), ^{
                [manager requestStateForRegion: region];
//            });
//        }
    });
}

@end
