//
//  RVLJSONTools.h
//  Reveal
//
//  Created by Bobby Skinner on 2/6/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (RVLJSONTools)

- (NSDictionary*)flattenedDictionary;

@end
