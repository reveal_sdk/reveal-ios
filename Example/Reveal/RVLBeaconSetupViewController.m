//
//  RVLBeaconSetupViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 7/26/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLBeaconSetupViewController.h"
#import "RVLBeaconTest.h"

@interface RVLBeaconSetupViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *useDebugBeacons;
@property (weak, nonatomic) IBOutlet UISwitch *useSecondaryBeaconMonitor;
@property (weak, nonatomic) IBOutlet UISlider *beaconCountSlider;
@property (weak, nonatomic) IBOutlet UILabel *beaconCountLabel;
@property (weak, nonatomic) IBOutlet UISwitch *duplicateRevealUUIDs;
@property (weak, nonatomic) IBOutlet UISlider *agressivenessSlider;
@property (weak, nonatomic) IBOutlet UITextView *beaconList;

@end

@implementation RVLBeaconSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)controlValueChanged:(id)sender {
    RVLBeaconTest* test = [RVLBeaconTest shared];
    
    test.useLocalBeaconGroups = self.useDebugBeacons.isOn;
    
    NSArray<NSString*>* list = [[self.beaconList text] componentsSeparatedByString: @"\n"];
    test.beaconGroupsForReveal = list;
    
    test.simBeaconCount = [self.beaconCountSlider value];
    
    test.includeLocalBeacons = [self.duplicateRevealUUIDs isOn];
    
    test.agressiveness = [self.agressivenessSlider value];
    
    test.useSim = [self.useSecondaryBeaconMonitor isOn];
    
    [self updateControls];
}

- (void) updateControls {
    
    RVLBeaconTest* test = [RVLBeaconTest shared];
    
    [self.useDebugBeacons setOn: test.useLocalBeaconGroups];
    
    NSMutableString* list = [NSMutableString string];
    for( NSString* item in test.beaconGroupsForReveal ) {
        if ( [item length] )
            [list appendFormat: @"%@\n", item];
    }
    [self.beaconList setText: list];
    
    [self.useSecondaryBeaconMonitor setOn: test.useSim];
    
    [self.beaconCountSlider setValue: test.simBeaconCount];
    [self.beaconCountLabel setText: [NSString stringWithFormat: @"%d",(int) test.simBeaconCount]];
    
    [self.duplicateRevealUUIDs setOn: test.includeLocalBeacons];
    
    [self.agressivenessSlider setValue: test.agressiveness];
}



- (void)textViewDidChange:(UITextView *)textView {
    RVLBeaconTest* test = [RVLBeaconTest shared];
    NSArray<NSString*>* list = [[self.beaconList text] componentsSeparatedByString: @"\n"];
    test.beaconGroupsForReveal = list;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
