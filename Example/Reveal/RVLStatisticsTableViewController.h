//
//  RVLStatisticsTableViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 12/14/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLStatisticsTableViewController : UITableViewController

- (void) reload;

@end
