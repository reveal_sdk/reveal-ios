//
//  RVLFilterOptionsViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 11/8/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import "RVLFilterOptionsViewController.h"
#import "RVLLogTableViewController.h"

@interface RVLFilterOptionsViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *reverseSwitch;
@property (weak, nonatomic) IBOutlet UILabel *typeFilterLabel;
@property (weak, nonatomic) IBOutlet UIStepper *typeFilterSelector;
@property (weak, nonatomic) IBOutlet UITextField *containsFilterText;

@property (nonatomic, strong, nullable) NSMutableArray* types;

@end

@implementation RVLFilterOptionsViewController

@synthesize parent = _parent;

- (void) setParent:(RVLLogTableViewController *)parent
{
    _parent = parent;
    
    [self readParent];
}

- (void) readParent {
    if ( _parent ) {
        NSOrderedSet* choices = [_parent types];
        self.types = [NSMutableArray arrayWithObject: @"All"];
        if ( choices.count > 0 ) {
            for( NSString* choice in choices )
                [self.types addObject: choice];
        }
        
        self.typeFilterSelector.maximumValue = [self.types count];
        
        if ( [[_parent type] length] ) {
            self.typeFilterLabel.text = [_parent type];
            self.typeFilterSelector.value = [self.types indexOfObject: [_parent type]] + 1;
        }
        else {
            self.typeFilterLabel.text = @"All";
            self.typeFilterSelector.value = 0;
        }
        
        self.reverseSwitch.on = _parent.reversed;
        self.containsFilterText. text = _parent.searchString;
    }
}

- (void) writeParent {
    if ( _parent ) {
        _parent.reversed = self.reverseSwitch.on;
        
        if ( [self.typeFilterLabel.text isEqualToString: @"All"] )
            _parent.type = nil;
        else
            _parent.type = self.typeFilterLabel.text;
        
        if ( [self.containsFilterText.text length] > 0 )
            _parent.searchString = self.containsFilterText.text;
        else
            _parent.searchString = nil;
        
        [_parent refreshTable];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    [self readParent];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)reverseSwitchPressed:(id)sender {
    [self writeParent];
}

- (IBAction)typeFilterSelectorPressed:(id)sender {
    NSInteger selection = self.typeFilterSelector.value;
    
    if ( selection < self.types.count )
        self.typeFilterLabel.text = self.types[selection];
    
    [self writeParent];
}

- (IBAction)containsFilterPressed:(id)sender {
    [self writeParent];
}

- (IBAction)donePressed:(id)sender {
    [self dismissViewControllerAnimated: YES completion: nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
