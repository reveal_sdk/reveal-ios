//
//  RVLViewController.m
//  Reveal
//
//  Created by Sean Doherty on 01/09/2015.
//  Copyright (c) 2014 StepLeader Digital. All rights reserved.
//

#import "RVLViewController.h"
#import "RVLBeaconTableViewCell.h"
#import "Reveal.h"
#import "RVLAppDelegate.h"
#import "RVLBeaconInformationViewController.h"
#import "RVLStatusPopupViewController.h"

#ifndef API_KEY
#define API_KEY                 @"53921489d442b031018220bf"
#endif

@import CoreLocation;

@interface RVLViewController ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<NSArray<NSObject*>*>* data;
@property (strong, nonatomic) NSArray<NSString*>* sectionNames;
@property (strong, nonatomic) NSTimer* timer;
@property (weak, nonatomic) IBOutlet UILabel *apiKeyLabel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *locationButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *webButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bluetoothButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *debugButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *scanningButton;

@property (nonatomic, weak, nullable) RVLStatusPopupViewController* popup;

@property (strong, nonatomic) UIColor* successColor;
@property (strong, nonatomic) UIColor* failColor;
@property (strong, nonatomic) UIColor* inProgressColor;

@end

@implementation RVLViewController

+ (NSString*)getTimeString:(NSTimeInterval)interval {
    // TODO: This copied time string code seems limited replace in the future,
    // but adequate for testing, this one will work unless you cross a month
    // boundry
    
    // Get the system calendar
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    
    // Create the NSDates
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] initWithTimeInterval: interval sinceDate:date1];
    
    // Get conversion to months, days, hours, minutes
    NSCalendarUnit unitFlags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitSecond;
    
    NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
    
    NSMutableString* result = [NSMutableString string];
    
    if ( [breakdownInfo day] > 0 )
        [result appendFormat: @"%dd ", (int) [breakdownInfo day]];
    
    if ( [breakdownInfo hour] >0 )
        [result appendFormat: @"%dh ", (int) [breakdownInfo hour]];
    
    if ( [breakdownInfo minute] >0 )
        [result appendFormat: @"%dm ", (int) [breakdownInfo minute]];
    
    if ( ([result length] == 0 ) || ( [breakdownInfo second] > 0 ) )
        [result appendFormat: @"%ds ", (int) [breakdownInfo second]];
    
    return result;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _successColor = [[UIColor alloc] initWithRed: 14.0/256.0 green: 122.0/256.0 blue: 254.0/256.0 alpha: 1.0];
    _failColor = [UIColor redColor];
    _inProgressColor =  [[UIColor alloc] initWithRed: 240.0/256.0 green: 230.0/256.0 blue: 6.0/256.0 alpha: 1.0];
    
    self.data = [NSMutableArray arrayWithCapacity: 4];
    
    for( int i=0 ; i<5 ; i++ )
        self.data[i] = @[];
    
    self.sectionNames = @[@"Pending Beacons", @"Networks", @"Resolved Beacons", @"Personas", @"Entries"];
    
    //[[Reveal sharedInstance] setDelegate: self];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(updateBeacons)
                                                 name: @"CurveFittedDistanceCalculator"
                                               object: nil];
    
    
#if NO_STARTUP_DIALOG != 1
    // delay to allow the main view to finish it's appearance before continuing
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"startupOptions" sender:self];
    });
#endif
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    self.addressLabel.text = @"searching...";
    self.apiKeyLabel.text = API_KEY;
    
    [self.tableView reloadData];
    [self.timer invalidate];
    self.timer = nil;
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval: 1.5
                                                  target: self
                                                selector: @selector( timerFired:)
                                                userInfo: nil
                                                 repeats: YES];
    
    [self updateStatuses];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(updateStatuses)
                                                 name: STATUS_UPDATED_NOTIFICATION
                                               object: nil];
    
    
    [self updateStatuses];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    //[[Reveal sharedInstance] setDelegate: self];
}

- (void) viewWillDisappear:(BOOL)animated
{
    //[[Reveal sharedInstance] setDelegate: nil];
    [super viewWillDisappear: animated];
}

- (void) timerFired:(id)sender
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressedStatusButton:(UIBarButtonItem *)sender {
    if ( sender == self.locationButton ) {
        [self setLastSelectedStatus: [[Reveal sharedInstance] getStatus: STATUS_LOCATION]];
    }
    
    if ( sender == self.bluetoothButton ) {
        [self setLastSelectedStatus: [[Reveal sharedInstance] getStatus: STATUS_BLUETOOTH]];
    }
    
    if ( sender == self.webButton ) {
        [self setLastSelectedStatus: [[Reveal sharedInstance] getStatus: STATUS_WEB]];
    }
    
    if ( sender == self.scanningButton ) {
        [self setLastSelectedStatus: [[Reveal sharedInstance] getStatus: STATUS_SCAN]];
    }
    
    // grab the view controller we want to show
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    UINavigationController* nav = [storyboard instantiateViewControllerWithIdentifier:@"StatusPopupNav"];
    self.popup = [[nav viewControllers] firstObject];
    
    if ( [self.popup isKindOfClass: [RVLStatusPopupViewController class]]  ) {
        self.popup.status = self.lastSelectedStatus;
        
        // present the controller
        // on iPad, this will be a Popover
        // on iPhone, this will be an action sheet
        nav.modalPresentationStyle = UIModalPresentationPopover;
        [self presentViewController: nav animated:YES completion:nil];
        
        // configure the Popover presentation controller
        UIPopoverPresentationController *popController = [nav popoverPresentationController];
        popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
        popController.barButtonItem = sender;
        popController.delegate = self;
    }
}

- (void) updateStatuses {
    RVLStatus* status = [[Reveal sharedInstance] getStatus: STATUS_BLUETOOTH];
    
    if ( status ) {
        if ( status.value == 0 )
            [self.bluetoothButton setImage: [UIImage imageNamed: @"ic_bluetooth_disabled"]];
        else
            [self.bluetoothButton setImage: [UIImage imageNamed: @"ic_bluetooth"]];
        
        switch ( status.value ) {
            case STATUS_INPROGRESS:
                [[self bluetoothButton] setTintColor: _inProgressColor];
                break;
                
            case STATUS_SUCCEED:
                [[self bluetoothButton] setTintColor: _successColor];
                break;
                
            default:
                [[self bluetoothButton] setTintColor: _failColor];
                break;
        }
    }
    
    status = [[Reveal sharedInstance] getStatus: STATUS_WEB];
    
    if ( status ) {
        if ( status.value == 0 )
            [self.webButton setImage: [UIImage imageNamed: @"ic_cloud_off"]];
        else
            [self.webButton setImage: [UIImage imageNamed: @"ic_cloud_queue"]];
        
        
        switch ( status.value ) {
            case STATUS_INPROGRESS:
                [[self webButton] setTintColor: _inProgressColor];
                break;
                
            case STATUS_SUCCEED:
                [[self webButton] setTintColor: _successColor];
                break;
                
            default:
                [[self webButton] setTintColor: _failColor];
                break;
        }
    }
        
    status = [[Reveal sharedInstance] getStatus: STATUS_WEB];
    
    if ( status ) {
        if ( status.value == 0 )
            [self.locationButton setImage: [UIImage imageNamed: @"ic_location_off"]];
        else
            [self.locationButton setImage: [UIImage imageNamed: @"ic_location_on"]];
        
        switch ( status.value ) {
            case STATUS_INPROGRESS:
                [[self locationButton] setTintColor: _inProgressColor];
                break;
                
            case STATUS_SUCCEED:
                [[self locationButton] setTintColor: _successColor];
                break;
                
            default:
                [[self locationButton] setTintColor: _failColor];
                break;
        }
    }
    
    status = [[Reveal sharedInstance] getStatus: STATUS_SCAN];
    
    if ( status ) {
        if ( status.value == 0 )
            [self.scanningButton setImage: [UIImage imageNamed: @"ic_perm_scan_wifi"]];
        else
            [self.scanningButton setImage: [UIImage imageNamed: @"ic_perm_scan_wifi"]];
        
        switch ( status.value ) {
            case STATUS_INPROGRESS:
                [[self scanningButton] setTintColor: [UIColor greenColor]];
                break;
                
            case STATUS_SUCCEED:
                [[self scanningButton] setTintColor: _successColor];
                break;
                
            default:
                [[self scanningButton] setTintColor: _failColor];
                break;
        }
    }
}

- (void) updateBeacons
{
    for( RVLBeacon* beacon in [[[Reveal sharedInstance] beacons] allValues] )
    {
        [beacon calculateDistance];
    }
    
    [self.tableView reloadData];
}

- (void) foundBeaconOfType:(NSString *)type identifier:(NSString *)identifier data:(NSDictionary *)data
{
    NSLog( @"Found beacon: %@", data );
    
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    NSDictionary* beacons = [[Reveal sharedInstance] beacons];
    
    self.data[0] = [[RVLDwellManager defaultManager] pendingEvents: RVLEventTypeBeacon];
    self.data[1] = [[RVLDwellManager defaultManager] pendingEvents: RVLEventTypeWiFiEnter];
    if ( beacons )
    {
        RVLAppDelegate* app = (RVLAppDelegate*) [[UIApplication sharedApplication] delegate];
        if ( [app beacons] )
            self.data[2] = [[[app beacons] reverseObjectEnumerator] allObjects];
    }
    
    NSArray* personas = [[Reveal sharedInstance] personas];
    
    if ( personas )
        self.data[3] = personas;
    else
        self.data[3] = @[];
    
    
    self.data[4] = [[RVLDwellManager defaultManager] pendingEvents: RVLEventTypeEnter];
    
    return [[self data] count];
}

//- (NSInteger) tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
//    NSInteger result = 44;
//    
//    switch ( [indexPath section] ) {
//        case 0:
//        case 2:
//            result = 88;
//            break;
//            
//        default:
//            result = 44;
//            break;
//    }
//    
//    NSLog( @"heightForRowAtIndexPath: %@=%d", indexPath, (int) result );
//    
//    return result;
//}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSArray* sectionData = self.data[section];
    NSArray* sectionData = self.data[section];
    
    return [sectionData count];
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( section < [self.sectionNames count] )
        return self.sectionNames[section];
    else
        return @"Error";
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"DetailCell";
    static NSString *beaconCellIdentifier = @"BeaconCell";
    NSArray* sectionData = self.data[[indexPath section]];
    
    UITableViewCell *cell = nil; // [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    
    RVLGenericEvent* event = sectionData[indexPath.row];
    NSString* foundTimeString = @"";
    NSString* sentTimeString = @"";
    NSString* secondsVisibleString = @"";
    
    if ( [event isKindOfClass: [RVLGenericEvent class]] ) {
        if ( [event discoveryTime] )
            foundTimeString = [NSString stringWithFormat: @" Found: %@ ago", [RVLViewController getTimeString: fabs( [[event discoveryTime] timeIntervalSinceNow] )]];
        
        if ( [event sentTime] )
            sentTimeString = [NSString stringWithFormat: @" Sent: %@ ago", [RVLViewController getTimeString: fabs( [[event sentTime] timeIntervalSinceNow] )]];
        
        if ( [event secondsVisible] > 0 )
            secondsVisibleString = [RVLViewController getTimeString: [event secondsVisible]];
    }
    
    if ( [event isKindOfClass: [RVLBeacon class]] ) {
        //RVLBeaconTableViewCell* beaconCell = [tableView cellForRowAtIndexPath: indexPath];
        RVLBeaconTableViewCell* beaconCell = [tableView dequeueReusableCellWithIdentifier: beaconCellIdentifier];
        
        RVLBeacon* item = (RVLBeacon*) event;
        
        if ( [item location] )
            beaconCell.addressLabel.text = [NSString stringWithFormat: @"%@", [item location]];
        else
            beaconCell.addressLabel.text = @"No address retrieved";
        
        NSMutableString* urlString = [NSMutableString string];
        
        NSData* payload = nil;
        if ( [item isKindOfClass: [RVLRawBeacon class]] ) {
            payload = [(RVLRawBeacon*)item  payload];
            
            if ( [(RVLRawBeacon*) item url] )
                [urlString appendString: [[(RVLRawBeacon*) item url] absoluteString]];
        }
        
        if ( payload || [item.decodedPayload length] ) {
            
            if ( [urlString length] )
                [urlString appendString: @" "];
            
            [urlString appendString: @"Payload: "];
            
            if ( [item.decodedPayload length] )
                [urlString appendFormat: @"\"%@\" ", item.decodedPayload];
                
            if ( payload )
                [urlString appendFormat: @"%@", payload];
        }
        
        
        beaconCell.identifierLabel.text = [NSString stringWithFormat: @"%@ %@", item.type, item.identifier];
        beaconCell.detailsLabel.text = [NSString stringWithFormat: @"%@ RSSI: %@%@%@",
                                        item.proximity, item.rssi, sentTimeString, foundTimeString];
        beaconCell.extrasLabel.text = [NSString stringWithFormat: @"%@", urlString];
        beaconCell.durationLabel.text = secondsVisibleString;
        
        if ( [item.accuracy floatValue] >= 0 )
        {
            double distance = [item proximityInMeters];
            
            if ( distance > 10000 )
                beaconCell.distanceLabel.text = @"\u221E";
            else
                beaconCell.distanceLabel.text = [NSString stringWithFormat: @"%0.1f", distance];
            
            switch ( item.proximityInteger )
            {
                case CLProximityFar:
                    beaconCell.backgroundColor = [UIColor colorWithRed: 1.0
                                                           green: 1.0
                                                            blue: 0.75
                                                           alpha: 0.50];
                    break;
                    
                case CLProximityUnknown:
                    beaconCell.backgroundColor = [UIColor colorWithRed: 1.0
                                                           green: 0.75
                                                            blue: 0.75
                                                           alpha: 0.50];
                    break;
                    
                default:
                    beaconCell.backgroundColor = [UIColor clearColor];
                    break;
            }
        }
        else
        {
            beaconCell.distanceLabel.text = @"N/A";
            
            beaconCell.backgroundColor = [UIColor colorWithRed: 1.0
                                                   green: 0.75
                                                    blue: 0.75
                                                   alpha: 0.25];

        }

        // NOTE This code is for debugging purposes - leave commented out
        if ( item.sentTime )
            beaconCell.identifierLabel.textColor = [UIColor blackColor];
        else if ( [item readyToSend] )
            beaconCell.identifierLabel.textColor = [UIColor greenColor];
        else
            beaconCell.identifierLabel.textColor = [UIColor blueColor];
        
        cell = beaconCell;
    }
    else if ( [event isKindOfClass: [NSString class]] ) {
        cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier: cellIdentifier];
        }
        
        NSString* item = (NSString*) event;
        
        cell.textLabel.text = [NSString stringWithFormat: @"%@", item];
        cell.detailTextLabel.text = nil;        // NOTE This code is for debugging purposes - leave commented out
        cell.textLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    
    if ( cell == nil )
    {
        cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
        
        cell.textLabel.text = [NSString stringWithFormat: @"%@", event];
        
        cell.detailTextLabel.text = @"Error no cell created for this event";
    }
    
    return cell;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ( [[segue destinationViewController] isKindOfClass: [RVLBeaconInformationViewController class]] ) {
        RVLBeaconInformationViewController* infoView = (RVLBeaconInformationViewController*) [segue destinationViewController];
        NSIndexPath* indexPath = [self.tableView indexPathForSelectedRow];
        
        if ( [indexPath section] < [self.data count] ) {
            NSArray* sectionData = self.data[[indexPath section]];
            
            if ( [indexPath row] < [sectionData count] ) {
                RVLBeacon* beacon = sectionData[[indexPath row]];
                
                if ( [beacon isKindOfClass: [RVLBeacon class]] ) {
                    infoView.beacon = beacon;
                }
            }
        }
    }
    else if ( [[segue destinationViewController] isKindOfClass: [RVLStatusPopupViewController class]] ) {
            self.popup = (RVLStatusPopupViewController*) [segue destinationViewController];
        
            self.popup.status = self.lastSelectedStatus;
        }
}

@end
