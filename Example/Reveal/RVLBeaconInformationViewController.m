//
//  RVLBeaconInformationViewController.m
//  Reveal
//
//  Created by Bobby Skinner on 4/21/17.
//  Copyright © 2017 StepLeader Digital. All rights reserved.
//

#import "RVLBeaconInformationViewController.h"

@implementation NSDictionary (stringAdd)

- (NSString* _Nonnull) string {
    return [self stringWithSeperator: @", "];
}

- (NSString* _Nonnull) stringWithSeperator:(NSString*  _Nonnull)sep {
    NSMutableString* result = [NSMutableString string];
    
    for( id key in self ) {
        id value = self[key];
        
        if ( [result length] > 0 )
            [result appendString: sep];
        
        if ( [value isKindOfClass: [NSString class]] )
            [result appendFormat: @"%@=\"%@\"", key, value];
        else if ( [value isKindOfClass: [NSDictionary class]] )
            [result appendFormat: @"%@={%@}", key, [(NSDictionary*)value string]];
        else if ( [value isKindOfClass: [NSArray class]] )
            [result appendFormat: @"%@=(%@)", key, [(NSDictionary*)value string]];
        else
            [result appendFormat: @"%@=%@", key, value];
    }
    
    return result;
}

@end

@implementation NSArray (stringAdd)

- (NSString* _Nonnull) string {
    return [self stringWithSeperator: @", "];
}

- (NSString* _Nonnull) stringWithSeperator:(NSString*  _Nonnull)sep {
    NSMutableString* result = [NSMutableString string];
    NSInteger key = 0;
    
    for( id value in self ) {
        
        if ( [result length] > 0 )
            [result appendString: sep];
        
        if ( [value isKindOfClass: [NSString class]] )
            [result appendFormat: @"[%d]=\"%@\"", (int) key, value];
        else if ( [value isKindOfClass: [NSDictionary class]] )
            [result appendFormat: @"[%d]={%@}", (int) key, [(NSDictionary*)value string]];
        else if ( [value isKindOfClass: [NSArray class]] )
            [result appendFormat: @"[%d]=(%@)", (int) key, [(NSDictionary*)value string]];
        else
            [result appendFormat: @"[%d]=%@", (int) key, value];
        key++;
    }
    
    return result;
}

@end

@interface RVLBeaconInformationViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation RVLBeaconInformationViewController

- (void) setBeacon:(RVLBeacon *)beacon {
    _beacon = beacon;
    
    self.device = _beacon.bluetooth;
}

- (void) setDevice:(RevealBluetoothObject *)device {
    _device = device;
    
    [self updateText];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateText];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) updateText {
    NSMutableString* text = [NSMutableString string];
    
    if ( self.beacon ) {
        [text appendString: @"BEACON:\n"];
        
        [text appendFormat: @"    Type: %@\n", _beacon.type];
        [text appendFormat: @"    ID: %@\n", _beacon.identifier];
        [text appendFormat: @"    Prox UUID: %@\n", _beacon.proximityUUID];
        [text appendFormat: @"    Proximity: %@\n", _beacon.proximity];
        [text appendFormat: @"    Accuracy: %@\n", _beacon.accuracy];
        [text appendFormat: @"    Major: %@\n", _beacon.major];
        [text appendFormat: @"    Minor: %@\n", _beacon.minor];
        
        if ( [self.beacon isKindOfClass: [RVLEddyStoneBeacon class]] ) {
            [text appendFormat: @"    URL: %@\n", ((RVLEddyStoneBeacon*)_beacon).url];
            [text appendFormat: @"    extended: %@\n", [((RVLEddyStoneBeacon*)_beacon).extendedData string]];
            
        }
        
        [text appendString: @"\n"];
    }
    
    if ( self.device ) {
        [text appendString: @"BLUETOOTH:\n"];
        
        [text appendFormat: @"    ID: %@\n", _device.identifier];
        [text appendFormat: @"    Name: %@\n", _device.name];
        
        if ( _device.advertisement )
            [text appendFormat: @"    Advertisement: %@\n", [_device.advertisement string]];
        
        if ( _device.services )
            [text appendFormat: @"    Services: %@\n", [_device.services string]];
        
        [text appendString: @"\n"];
    }
    
    [self.textView setText: text];
}

@end
