//
//  RVLFilterViewController.h
//  Reveal
//
//  Created by Bobby Skinner on 7/11/16.
//  Copyright © 2016 StepLeader Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLFilterViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, copy, nullable) NSArray<NSString*>* filterFields;
@property (nonatomic, copy, nullable) BOOL (^completed)(NSString * _Nullable field, NSString* _Nullable value );

@end
