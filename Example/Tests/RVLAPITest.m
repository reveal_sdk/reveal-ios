//
//  RVLAPITest.m
//  Reveal
//
//  Created by Bobby Skinner on 3/24/16.
//  Copyright © 2016 Sean Doherty. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Reveal.h"

// This rather convoluded set of #defines allows you to pass in environment variables
// for buddy build but not require them if you're not.
//
// Due to the XCode mechanism provided, you will get directive defined as empty instead
// of undefined if there is no environment variable defined. This is seems overly complex
// for such a simple task, but it handles all of the cases as far as I can see.
#define STRINGIFY(str) @#str
#define EXPAND_STRING(str) STRINGIFY(str)
#define NO_OTHER_MACRO_STARTS_WITH_THIS_NAME_
#define IS_EMPTY(name) defined(NO_OTHER_MACRO_STARTS_WITH_THIS_NAME_ ## name)
#define EMPTY(name) IS_EMPTY(name)

// has a key been provided?
#ifdef REVEAL_API_KEY
#if !(EMPTY(REVEAL_API_KEY) == 1)
#ifndef API_KEY
#define API_KEY                 EXPAND_STRING(REVEAL_API_KEY)
#endif
#endif
#endif

// has a service type been provided? If not assume they want the sandbox
#ifndef REVEAL_SERVER_TYPE
#define REVEAL_SERVER_TYPE      RVLServiceTypeSandbox
#elif EMPTY(REVEAL_SERVER_TYPE) == 1
#undef REVEAL_SERVER_TYPE
#define REVEAL_SERVER_TYPE      RVLServiceTypeSandbox
#endif

// if no key was created above then use the default
#ifndef API_KEY
#define API_KEY                 @"NO KEY DEFINED"
#endif

@interface RVLAPITest : XCTestCase

@end

@implementation RVLAPITest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// TODO: Remove this once we finalize the buddy build process, but it appears that they do
//       Not set the environment variables on the simulator or device in testing so the compile time
//       alternative appears to be the right way

//- ( void) testRuntimeKeySDKSetup {
//    // get api key from an environment variable, if not provited use the default
//    NSString *apiKey = [[[NSProcessInfo processInfo] environment] objectForKey:@"REVEAL_API_KEY"];
//    if ( [apiKey length] == 0 )
//        apiKey = API_KEY;
//    
//    // get the service type from an environment variable or use the default if not a valid one
//    RVLServiceType serviceType = REVEAL_SERVER_TYPE;
//    NSString *serverType = [[[[NSProcessInfo processInfo] environment] objectForKey:@"REVEAL_SERVER_TYPE"] lowercaseString];
//    if ( [serverType isEqualToString: @"rvlservicetypeproduction"] )
//        serviceType = RVLServiceTypeProduction;
//    else if ( [serverType isEqualToString: @"rvlservicetypesandbox"] )
//        serviceType = RVLServiceTypeSandbox;
//    
//    NSLog( @"Starting with API key: \"%@\"", apiKey );
//    
//    if ( [apiKey isEqualToString: API_KEY] )
//    {
//        XCTFail( @"We do not have an runtime APIKey" );
//    }
//    else
//    {
//        XCTAssert( true, "API KEY: %@", apiKey );
//    }
//    
//    // Get a reference to the SDK object
//    Reveal *theSDK = [[Reveal sharedInstance] setupWithAPIKey:apiKey andServiceType: serviceType];
//}

- (void)testKeyPassage {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    NSString* apiKey =  EXPAND_STRING( REVEAL_API_KEY );
    
    if ( [apiKey length] > 3 )
    {
        XCTAssert( true, "API KEY: %@", apiKey );
    }
    else
    {
        XCTFail( @"We do not have an APIKey" );
    }
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
