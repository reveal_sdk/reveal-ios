#!/bin/sh
echo "Starting framework build"
pwd

CONFIGURATION=Release
SDK_VER=11.1
LIBNAME=libRevealLibrary.a
FRAMEWORK=Reveal

SHARED_FLAGS="-project RevealFramework.xcodeproj -configuration $CONFIGURATION -scheme RevealLibrary"

# find output dirs
export `xcodebuild $SHARED_FLAGS -showBuildSettings -sdk iphoneos$SDK_VER |grep " BUILT_PRODUCTS_DIR ="|head -1|tr -d ' '`
export IPHONEOS_DIR=$BUILT_PRODUCTS_DIR

export `xcodebuild $SHARED_FLAGS -showBuildSettings -sdk iphonesimulator$SDK_VER |grep " BUILT_PRODUCTS_DIR ="|head -1|tr -d ' '`
export IPHONESIMULATOR_DIR=$BUILT_PRODUCTS_DIR

rm -r -f $IPHONESIMULATOR_DIR/*
rm -r -f $IPHONEOS_DIR/*

# clean targets
xcodebuild $SHARED_FLAGS -sdk iphonesimulator$SDK_VER clean
xcodebuild $SHARED_FLAGS -sdk iphoneos$SDK_VER clean

# build targets 
xcodebuild $SHARED_FLAGS -sdk iphonesimulator$SDK_VER -destination 'generic/platform=iOS Simulator,OS=latest'
xcodebuild $SHARED_FLAGS -sdk iphoneos$SDK_VER -destination 'generic/platform=iOS,OS=latest'

echo "Built libraries"
ls -la $IPHONEOS_DIR/$LIBNAME
ls -la $IPHONESIMULATOR_DIR/$LIBNAME

echo "iphone dir: $IPHONEOS_DIR"
ls -la $IPHONEOS_DIR

echo "iphone sim dir: $IPHONESIMULATOR_DIR"
ls -la $IPHONESIMULATOR_DIR

file $IPHONEOS_DIR/$LIBNAME | head -1
file $IPHONESIMULATOR_DIR/$LIBNAME | head -1

# build framework
export INSTALL_DIR=Frameworks/$FRAMEWORK.framework

rm -rf $INSTALL_DIR
mkdir -p $INSTALL_DIR
mkdir -p $INSTALL_DIR/Versions/A/Headers

xcrun -sdk iphoneos lipo -create $IPHONEOS_DIR/$LIBNAME $IPHONESIMULATOR_DIR/$LIBNAME -o $INSTALL_DIR/Versions/A/$FRAMEWORK

# Copy the header files to the final product folder.
cp -r $IPHONEOS_DIR/include/ $INSTALL_DIR/Versions/A/Headers/

# Copy Resources
mkdir -p $INSTALL_DIR/Versions/A/Resources
#cp -r $IPHONEOS_DIR/resources/ $INSTALL_DIR/Versions/A/Resources/

open "$INSTALL_DIR/.."

#create relative symbolic link.
cd $INSTALL_DIR

#link
ln -s A Versions/Current
ln -s Versions/A/$FRAMEWORK $FRAMEWORK
#ln -s Versions/A/Resources Resources
ln -s Versions/A/Headers Headers

xcodebuild -showsdks
