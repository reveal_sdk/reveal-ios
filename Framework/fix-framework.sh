#!/bin/sh

#  fix-framework.sh
#  RevealFramework
#
#  Created by Sean Doherty on 1/11/18.
#  Copyright © 2018 Reveal Mobile. All rights reserved.

binary="${TARGET_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/Reveal.framework/Reveal"
archs="$(lipo -info "$binary" | rev | cut -d ':' -f1 | rev)"
stripped=""
for arch in $archs; do
if ! [[ "${VALID_ARCHS}" == *"$arch"* ]]; then
    # Strip non-valid architectures in-place
    lipo -remove "$arch" -output "$binary" "$binary" || exit 1
    stripped="$stripped $arch"
fi
done
if [[ "$stripped" ]]; then
    echo "Stripped $binary of architectures:$stripped"
fi
